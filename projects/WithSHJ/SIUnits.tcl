#================================================================
# Units.tcl-- define system of units
# Zhaohui Yang and Boris Jeremic (@ucdavis.edu)
# Modified by Yong Li 2010 August 2nd
#================================================================
set LunitTXT "m";			# define basic-unit text for output
set FunitTXT "N";			# define basic-unit text for output
set TunitTXT "sec";			# define basic-unit text for output

#Basic units
set m   1.0; # meter for length
set sec 1.0; # second for time
set kg  1.0; # Kilogram for mass

#Define Constant Parameters
set Inf 1.e10; 			        # a really large number
set Null [expr 1/$Inf]; 		# a really small number

# angle
set rad 1.0;
set pi [expr 2*asin(1.0)]; 		# define constants
set PI [expr $pi];
set deg [expr $pi/180.0*$rad];

# length
set cm  [expr 0.01*$m];
set mm  [expr 0.001*$m];

# mass
set ton [expr 1000.0*$kg]
set g [expr 9.81*$m/pow($sec, 2)];	
set G [expr $g];	

# force
set N   [expr $kg * $m / ($sec * $sec)] ;
set kN [expr 1000.0*$N];

# pressure
set Pa  [expr 1.0*$N/pow($m, 2)];
set kPa [expr 1000.0*$Pa];
set MPa [expr 1000.0*$kPa];
set GPa [expr 1000.*$MPa];

# ###################### Converted Units  ##########################  

# length
set in  [expr 0.0254*$m];
set ft [expr 12.0*$in];

#Area and Volume Units 
set in2 [expr $in*$in]; 		# inch^2
set in4 [expr $in*$in*$in*$in]; 		# inch^4

# mass
set lbs [expr 0.453592*$kg];
set g [expr 32.174*$ft/pow($sec,2)]; 	# gravitational acceleration

# force 
set lbf [expr $lbs*$g];
set kip [expr 1000.0*$lbf];

# pressure or force density
set plf [expr $lbf/pow($ft,1)];
set klf [expr 1000*$plf];
set ksi [expr $kip/pow($in,2)];
set psi [expr $ksi/1000.];
set psf [expr $lbf/pow($ft,2)];		# pounds per square foot
set pcf [expr $lbf/pow($ft,3)];	    # pcf = #/cubic foot

