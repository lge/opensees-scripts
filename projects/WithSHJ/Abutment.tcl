proc Abutment {fcn tag {abutArgs {}} } {

if { [string compare $fcn "create"] == 0 } {
	# parse abutArgs
	set shift [lindex $abutArgs 0]
	set type [lindex $abutArgs 1]

	# abutments use tagged objects between 6001 and 8000, gives them 200 tagged objects each
	set ashift 600001
	set aeach 200
	set base_shift [expr $shift+$ashift+($tag-1)*$aeach]
	
	# if the procedure already exists in tcl space, can recreate the OpenSees domain without
	# redefining the tcl procedure
	set tl [info command AbutObject$tag]
	if { [llength $tl] == 0 } {
		# create modules
		${type}::${type} AbutObject$tag $tag $base_shift [lrange $abutArgs 2 end]
	}
	
    # initialize modules
    AbutObject$tag "initialize"
    
} else {
	# pass method to object for evaluation, no longer depends on the type
	if { [llength $abutArgs] > 0 } {
		set outp [AbutObject$tag $fcn $abutArgs]
	} else {
		puts "I am here"
		set outp [AbutObject$tag $fcn]
		
	}
    return $outp
}

}
