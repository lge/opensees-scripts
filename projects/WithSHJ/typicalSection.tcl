#  Section Model for typical Pier for California HighSpeed Rail Bridge;
#  March 15th 2012. Yong Li, at University of California, San Diego;
#  Under the guidance of Jackson Thomas at Parsons Brinkerhoff;

	###################################################################################################################
	# BUILD FIBER SECTION
	###################################################################################################################
	
	set Ubig 1.e10; 				# a really large number
	# MATERIAL parameters -------------------------------------------------------------------
	set IDconcCore 4; 				# material ID tag -- confined core concrete
	set IDconcCover 5; 				# material ID tag -- unconfined cover concrete
	set IDreinf 6; 					# material ID tag -- reinforcement
	set IDSteelTot 9;
	
	# nominal concrete compressive strength
	set fc 		[expr -4.0];			        # ksi, CONCRETE Compressive Strength  (+Tension, -Compression)
	set Ec 		[expr 57.0*sqrt(-$fc*1000.0)];	# ksi, Concrete Elastic Modulus
	
	# UNCON CONCRETE #
	set fc1U 		$fc;			# UNCONFINED concrete (todeschini parabolic model), maximum stress
	set eps1U		-0.002;			# strain at maximum strength of unconfined concrete
	set fc2U 		[expr 0.0*$fc1U];	# ultimate stress [expr 0.2*$fc1U]
	set eps2U		-0.005;			# strain at ultimate stress
	set lambda 0.1;					# ratio between unloading slope at $eps2 and initial slope $Ec
	uniaxialMaterial Concrete01 $IDconcCover $fc1U $eps1U $fc2U $eps2U; 			# build cover concrete (unconfined)

	
	# CON CONCRETE #
	set Kfc 		1.375;			# ratio of confined to unconfined concrete strength
	set fc1C 		[expr $Kfc*$fc];	# CONFINED concrete (mander model), maximum stress (-5.705)[expr $Kfc*$fc]
	set eps1C		-0.0065;		# strain at maximum stress (-0.004384)[expr 2.*$fc1C/$Ec]
	set fc2C 		-4.5;			# ultimate stress (-5.0)[expr 0.2*$fc1C]
	set eps2C 		-0.0266;		# strain at ultimate stress(-0.01836)[expr 5*$eps1C]
	# tensile-strength properties
	set ftC [expr -0.1*$fc1C];			# tensile strength +tension
	set ftU [expr -0.1*$fc1U];			# tensile strength +tension
	set Ets [expr $ftU/0.002];			# tension softening stiffness
	#uniaxialMaterial Concrete01 $IDconcCore $fc1C $eps1C $fc2C $eps2C 			# build core concrete (confined)
	uniaxialMaterial Concrete02 $IDconcCore $fc1C $eps1C $fc2C $eps2C $lambda $ftC $Ets;	# build core concrete (confined)

	
	# STEEL #
	set Fy 		68.0;				# ksi, STEEL yield stress
	set Es		29000.0;			# ksi, modulus of steel
	set Bs		0.01;				# strain-hardening ratio 
	set R0 		15;				# control the transition from elastic to plastic branches
	set cR1		0.925;				# control the transition from elastic to plastic branches
	set cR2 	0.15;				# control the transition from elastic to plastic branches
	uniaxialMaterial Steel02 $IDreinf $Fy $Es $Bs $R0 $cR1 $cR2;				# build reinforcement material
	#uniaxialMaterial MinMax $matTag $otherTag <-min $minStrain> <-max $maxStrain>
	uniaxialMaterial MinMax $IDSteelTot $IDreinf -min -0.09 -max 0.09

	# section GEOMETRY -------------------------------------------------------------
	set DSec 		[expr 8.0*12.0]; 	# ft, Column Diameter
	set coverSec 		[expr 2.0];		# in, Column cover to reinforcing steel NA.
	set numBarsSec 		47;			# number of uniformly-distributed longitudinal-reinforcement bars
	set barAreaSec 		[expr 3.12];		# in^2, area of longitudinal-reinforcement bars
	set SecTag 		1;			# set tag for symmetric section
	
	# 
	set ri 0.0;			# inner radius of the section, only for hollow sections
	set ro [expr $DSec/2.0];	# overall (outer) radius of the section
	set nfCoreR 10;			# number of radial divisions in the core (number of "rings")
	set nfCoreT 20;			# number of theta divisions in the core (number of "wedges")
	set nfCoverR 1;			# number of radial divisions in the cover
	set nfCoverT 20;		# number of theta divisions in the cover
	
	# Define the fiber section
	section fiberSec $SecTag  {
	    set rc [expr $ro-$coverSec];							# Core radius
	    patch circ $IDconcCore $nfCoreT $nfCoreR 0. 0. $ri $rc 0 360;			# Define the core patch
	    patch circ $IDconcCover $nfCoverT $nfCoverR 0. 0. $rc $ro 0 360;		# Define the cover patch
	    set theta [expr 360.0/$numBarsSec];						# Determine angle increment between bars
	    layer circ $IDSteelTot $numBarsSec $barAreaSec 0. 0. $rc $theta 360;		# Define the reinforcing layer
	}
	
	# assign torsional Stiffness for 3D Model
	set SecTagTorsion 99;								# ID tag for torsional section behavior
	set SecTag3D 3;									# ID tag for combined behavior for 3D model
	uniaxialMaterial Elastic $SecTagTorsion $Ubig;					# define elastic torsional stiffness
	section Aggregator $SecTag3D $SecTagTorsion T -section $SecTag;			# combine section properties
	
	

 
