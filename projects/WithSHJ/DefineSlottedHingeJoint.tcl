#proc DefineSlottedHingeJoint { } {
	##########################################################
	#  DefineSlottedHingeJoint $LoadTag $GMdirection  $GMSineAccAmpl  $TPeriodSine $DurationSine
	##########################################################
	# By Yong Li, foxchameleon@gmail.com
	# Project: California High Speed Rail Project
	#  Define Slotted Hinge Joint under the help of Roy A. Imbsen
	#
	set matSHJNO 200;
	
	# Define the longitudinal force-deformation properties
	set delta_C [expr 0.4*$in];
	set delta_T [expr 0.4*$in];
	set SHJ_Fx_C [expr 650*$kip];
	set SHJ_Fx_T [expr 650*$kip];
	set SHJ_Kx_C [expr $SHJ_Fx_C/(0.3*$in)];
	set SHJ_Kx_T [expr $SHJ_Fx_T/(0.3*$in)];
	set eta_C    0.01;
	set eta_T    0.01;
	set SHJ_damageType "damage";
	
	set impulseStiffness [expr 10*$A_deckEndDiaphragm*0.5*$E_deck/($typicalSpanLength*$numSpanContinuous)];
	set delta_Gap        [expr $gapSEJ];
	
	set virtualSpringRatio 0.0000001;
	
	
	uniaxialMaterial ElasticPPGap [expr $matSHJNO+10+1] $SHJ_Kx_T $SHJ_Fx_T $delta_T $eta_T $SHJ_damageType;
	uniaxialMaterial ElasticPPGap [expr $matSHJNO+10+2] $SHJ_Kx_C [expr -$SHJ_Fx_C] [expr -$delta_C] $eta_C $SHJ_damageType;
	uniaxialMaterial Elastic [expr $matSHJNO+10+3] [expr $virtualSpringRatio*$SHJ_Kx_T]; # Vritual Spring to get rid of zero stiffness for the gap to get better convergence
	uniaxialMaterial Parallel [expr $matSHJNO+1] [expr $matSHJNO+10+1] [expr $matSHJNO+10+2] [expr $matSHJNO+10+3];
	
	# Define the trasversal force-deformation properties
	set SHJ_Ky   1e10;
	uniaxialMaterial Elastic [expr $matSHJNO+2] [expr $SHJ_Ky];
	
	# Define the vertical force-deformation properties
	set SHJ_Kz   1e10;
	uniaxialMaterial Elastic [expr $matSHJNO+3] [expr $SHJ_Kz];
	# Define the moment-rotation properties around longitudinal axis  (Twist)
	
	
	# Define the moment-rotation properties around transversal axis
	
	
	# Define the moment-rotation properties around vertial axis
	
	# Define the element
	## Define elements in the model files...
	##
#}


