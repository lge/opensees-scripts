proc DefineLoadPattern_LateralLoad { LoadTag LateralDOF iLateralNodeID iFj}  {
	###########################################################################
	## AnalyzeStatic $iDmax $IDctrlNode $IDctrlDOF $DincrStatic $iLateralNodeID $iFj $CycleType $Ncycles $Fact
	###########################################################################
	# define a lateral-load pattern
	# by Silvia Mazzoni, 2008
	# input variables
	# $LoadTag:	unique load tag
	# LateralDOF: DOF where load is applied
	#	$iLateralNodeID	: node ID tag where lateral loads iFj are applied
	# 	$iFj		: lateral-load vector -- ALL are applied in direction of IDctrlDOF
	pattern Plain $LoadTag Linear {;			# define load pattern
		foreach LateralNodeID $iLateralNodeID Fj $iFj {
			set loadvetto "0.0 0.0 0.0 0.0 0.0 0.0 "
			lset loadvetto [expr $LateralDOF-1] $Fj
			eval "load   $LateralNodeID $loadvetto"
		}
	};		# end load pattern
	return
}