proc AnalyzeDisplacementHistory { iDmax IDctrlNode IDctrlDOF args}  {;			
	###########################################################################
	## AnalyzeDisplacementHistory $iDmax $IDctrlNode $IDctrlDOF
	##  or
	## AnalyzeDisplacementHistory $iDmax $IDctrlNode $IDctrlDOF -CycleType $CycleType  -Ncycles $Ncycles -DisplacementScaleFactor $DisplacementScaleFactor \
	## 						-DisplacementIncrement $DisplacementIncrement -analysisType $analysisType -constraintsType $constraintsType -alphaSP $alphaSP -alphaMP $alphaMP \
	## 						-numbererType $numbererType -systemType $systemType -testType $testType  -maxNumIter $maxNumIter -printFlag $printFlag -Tolerance $Tolerance\
	## 						-NstepReduceIncrement $NstepReduceIncrement -maxNumIterConverge $maxNumIterConverge -printFlagConverge $printFlagConverge \
	##						-algorithmType $algorithmType -NewtonLineSearchRatio $NewtonLineSearchRatio -algorithmCount $algorithmCount"
	##  or
	## set AnalysisData "CycleType $CycleType  Ncycles $Ncycles DisplacementScaleFactor $DisplacementScaleFactor \
	## 						DisplacementIncrement $DisplacementIncrement analysisType $analysisType constraintsType $constraintsType alphaSP $alphaSP alphaMP $alphaMP \
	## 						numbererType $numbererType systemType $systemType testType $testType  maxNumIter $maxNumIter printFlag $printFlag Tolerance $Tolerance\
	## 						NstepReduceIncrement $NstepReduceIncrement maxNumIterConverge $maxNumIterConverge printFlagConverge $printFlagConverge \
	##						algorithmType $algorithmType NewtonLineSearchRatio $NewtonLineSearchRatio algorithmCount $algorithmCount"
	## AnalyzeDisplacementHistory $iDmax $IDctrlNode $IDctrlDOF -AnalysisData $AnalysisData
	###########################################################################
	# perform displacement-controlled static analysis (pushover or cyclic) based on previously-defined load pattern
	# From Tcl Manual: If the last formal argument has the name args, then a call to the procedure may contain more actual arguments than the procedure has formals. 
	#         In this case, all of the actual arguments starting at the one that would be assigned to args are combined into a list (as if the list command had been used); 
	#         this combined value is assigned to the local variable args. 
	# by Silvia Mazzoni, 2006
	# input variables
	#	$iDmax	 : vector defining maximum peaks for static analysis, or filename for input.
	#	$IDctrlNode	: node tag where Dmax is checked
	#	$IDctrlDOF	: dof tag for Dmax

	# set up a few default values. these will be over-run when they are defined as optional values at the end of the list.
	set DefaultDisplacementHistoryAnalysisModelData "CycleType Push Ncycles 1 DisplacementScaleFactor 1\
			 DisplacementIncrement 0.01 analysisType Static constraintsType Transformation  alphaSP 1e6 alphaMP 1e6 \
			 numbererType RCM systemType BandGeneral testType EnergyIncr maxNumIter 6 printFlag 0 Tolerance 1e-8\
			NstepReduceIncrement 10 maxNumIterConverge 2000 printFlagConverge 0 \
			algorithmType Newton NewtonLineSearchRatio 0.8 algorithmCount 5"
	foreach {Name DefaultValue} $DefaultDisplacementHistoryAnalysisModelData {
		set $Name $DefaultValue
	}
	
	# NOTE: check for rigid diaphragms. if they are used, you should use Lagrange Constraints. Transformation is good for large models.
	
	# override defaults with anything specified in argument list:
	foreach {Option Value} $args {
		if {[string index $Option 0]=="-"} {;							# remove the hyphen
			set Option [string range $Option 1 end]
		}
		set $Option $Value
		if {[llength $Value]>1} {;											# if the argument is a list
			foreach {SubName SubValue} $Value {
				if {[string index $SubName 0]=="-"} {;				# remove the hyphen
					set SubName [string range $SubName 1 end]
				}
				set $SubName $SubValue
			}
		}
	}

# -----------------------------------------------
	set fmt1 "%s analysis: Control-Node Disp =%.4f";	# format for screen/file output of DONE/PROBLEM analysis
	set ConvergeStaticAnalysisCommands {;					# evaluate these commands to when having convergenct problems. key: the NewTolerance variable.
			puts "Trying Newton with Initial Tangent .."
			test $testType $NewTolerance	$maxNumIterConverge $printFlagConverge 
			algorithm Newton -initial
			set ok [analyze 1]
			test $testType $Tolerance	$maxNumIter    $printFlag 
			algorithm $algorithmType
		if {$ok != 0} {
			puts "Trying Broyden .."
			algorithm Broyden 8
			set ok [analyze 1 ]
			algorithm $algorithmType
		}
		if {$ok != 0} {
			puts "Trying NewtonWithLineSearch .."
			algorithm NewtonLineSearch $NewtonLineSearchRatio 
			set ok [analyze 1]
			algorithm $algorithmType
		}
		if {$ok == 0} {puts "Converged at this step, continuing...."}
		set returnValue $ok
	}

# -------------------------------------------------- clean analysis model and results -----------------------
	wipeAnalysis
# ---------------------------------------------------------------------------------------------------------------------

	if {$constraintsType == "Plain" | $constraintsType == "Transformation"} {;			# CONSTRAINTS
		constraints $constraintsType ;	# Plain & Transformation constraints
	} else {
		constraints $constraintsType $alphaSP $alphaMP ;	# Penalty & Lagrange congs
	}

	if {$systemType  == "SparseGeneralPivot"} {;						# SYSTEM
		system SparseGeneral  -piv;	# optional pivoting for SparseGeneral system
	} else {
		system $systemType ; 
	}
	
	numberer $numbererType;																# renumber dof's to minimize band-width (optimization), if you want to;	# NUMBERER
	test $testType $Tolerance $maxNumIter $printFlag; 										# tolerance, max no. of iterations, and print code , 1: every iteration;
	
	if {$algorithmType == "BFGS" | $algorithmType == "Broyden"	} {;					# ALGORITHM
		algorithm $algorithmType $algorithmCount;	# use Newton's solution algorithm: updates tangent stiffness at every iteration
	} elseif { $algorithmType == "NewtonLineSearch"} {
		algorithm $algorithmType $NewtonLineSearchRatio;
	} else {
		algorithm $algorithmType;											# use Newton's solution algorithm: updates tangent stiffness at every iteration
	}

    integrator DisplacementControl $IDctrlNode $IDctrlDOF 0.0;			
	analysis Static

	# -----------------------------------------------------------------------------
	if {[llength $iDmax]==1} {;										# if iDmax not a list of peaks
		if {[catch {set temp [expr $iDmax + 1]}]} {;			# and iDmax is not an individual number
			# -------------- reading peaks from a file-------# then it is a filename.
			set Filename $iDmax
			set iDmax ""
			set Dmax0 0.0
			# Open the input file and catch the error if it can't be read
			if [catch {open $Filename r} inFileID] {
				puts stderr "Cannot open $Filename for reading"
				return -1
			} else {
				# Look at each line in the file
				foreach line [split [read $inFileID] \n] {
					if {[llength $line] == 0} {
						continue;
					} else {
						set Dmax1 [lindex $line end];					# just in case there is something in the first columns....
						set Dstep [expr $Dmax1 - $Dmax0];
						lappend iDmax $Dstep
					}
					set Dmax0 $Dmax1
				}
			}
			close $inFileID
		}
	}
	# -----------------------------------------------------------------------------------------------------------
	set ok 0;
	foreach Dmax $iDmax {
		set iDstep ""
		# ------------------- generate a list (iDstep) of incremented displacements for this peak value -------------
		set Disp 0.
		lappend iDstep $Disp
		lappend iDstep $Disp
		set Dmax [expr $Dmax*$DisplacementScaleFactor];	# scale value
	
		if {$Dmax<0} {
			set dx [expr -$DisplacementIncrement]
		} else {
			set dx $DisplacementIncrement;
		}
		set NstepsPeak [expr int(abs($Dmax)/$DisplacementIncrement)]
		for {set i 1} {$i <= $NstepsPeak} {incr i 1} {;		# zero to one
			set Disp [expr $Disp + $dx]
			lappend iDstep $Disp
		}
		lappend iDstep $Dmax
		if {[string range $CycleType 0 3] !="Push"} {
			for {set i 1} {$i <= $NstepsPeak} {incr i 1} {;		# one to zero
				set Disp [expr $Disp - $dx]
				lappend iDstep $Disp;			# append to file
			}
			lappend iDstep 0.0;			# append exact value
			if {[string range $CycleType 0 3] !="Half"} {
				for {set i 1} {$i <= $NstepsPeak} {incr i 1} {;		# zero to minus one
					set Disp [expr $Disp - $dx]
					lappend iDstep $Disp;			# write to file
				}
				lappend iDstep -$Dmax;				# write to file
				for {set i 1} {$i <= $NstepsPeak} {incr i 1} {;		# minus one to zero
					set Disp [expr $Disp + $dx]
					lappend iDstep $Disp;			# write to file
				}
				lappend iDstep 0.0;			# write to file
			}
		}
		# -------------------------------------------------------------
		
		
		for {set i 1} {$i <= $Ncycles} {incr i 1} {
			set zeroD 0
			set D0 0.0
			foreach Dstep $iDstep {
				set D1 $Dstep
				set Increment [expr $D1 - $D0]
				# ----------------------------------------------analysis parameters----------------------------
				integrator DisplacementControl  $IDctrlNode $IDctrlDOF $Increment;							# INTEGRATOR	# TEST
				# ----------------------------------------------check if GUI has tried to stop the analysis------------------------
				global StopAnalysisSwitch
				update
				if {[info exist StopAnalysisSwitch]} {
					if {$StopAnalysisSwitch == "yes"} {
						return
					}
				};
				# ----------------------------------------------first analyze command------------------------
				set ok [analyze 1]
				# ----------------------------------------------if convergence failure-------------------------
				if {$ok != 0} {
					set NewTolerance $Tolerance
					set ok [eval $ConvergeStaticAnalysisCommands];	# try some other analysis parameters
					if {$ok != 0} {
						set Nk $NstepReduceIncrement;
						for {set k 1} {$k <= $Nk} {incr k 1} {
							integrator DisplacementControl  $IDctrlNode	$IDctrlDOF [expr $Increment/$Nk]
							set ok [analyze 1]
							if {$ok != 0} {
								set NewTolerance $Tolerance
								set ok [eval $ConvergeStaticAnalysisCommands];	# try some other analysis parameters
								if {$ok != 0} {
									set NewTolerance [expr $Tolerance*100];		# increase tolerance
									set ok [eval $ConvergeStaticAnalysisCommands];	# try some other analysis parameters
									if {$ok != 0} {
										set putout [format $fmt1 "PROBLEM"  [nodeDisp $IDctrlNode $IDctrlDOF]]
										puts $putout
										return -1
									};	# end if
								};	# end if
							};	# end if 
						};	# end k-Nk
					};	# end if 
					if {$ok == 0} {puts "Converged at this step, continuing...."}
				};	# end if 
				# -----------------------------------------------------------------------------------------------------
				catch {DisplayAnalysisDisplays}		;			# run this proc only if it has been defined, continue w/o error otherwise
				set D0 $D1;			# move to next step
			}; 	# end Dstep
		};	# end i
	};	# end of iDmax

	# announce if we made it over through the analysis:
	if {$ok != 0 } {
		set AnalysisResults [format $fmt1 "PROBLEM" [nodeDisp $IDctrlNode $IDctrlDOF]]
	} else {
		set AnalysisResults [format $fmt1 "DONE"  [nodeDisp $IDctrlNode $IDctrlDOF]]
	}
# 	puts $AnalysisResults;	# print results to screen
	return 0
}