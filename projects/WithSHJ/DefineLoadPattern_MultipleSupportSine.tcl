proc DefineLoadPattern_MultipleSupportSine {LoadTag iSupportNode iGMdirection iGMSineDispAmpl  iTPeriodSine iDurationSine args } {
	#############################################################
	#  DefineLoadPattern_MultipleSupportSine $LoadTag $iSupportNode $iGMdirection  $iGMSineDispAmpl  $iTPeriodSine $iDurationSine
	#############################################################
	# define load pattern for ground-motion analysis -- multiple-support Sine-Wave excitation, unidirectional & bidirectional
	# define nodal displacements for Multiple-support excitation
	#	   Silvia Mazzoni, 2008 (mazzoni@berkeley_NO_SPAM_.edu)
	##   $LoadTag:	unique load tag
	##   iSupportNode		# Node ID's where motion is imposed
	##   iGMdirection 		# lateral dof for ground motion input
	##   iGMSineAccAmpl	# amplitude of input sine-wave acceleration
	##   iTPeriodSine		# period of input sine-wave acceleration
	##   iDurationSine		# duration of each sine-wave motion
	#
   	set PI [expr 2*asin(1.0)];

	# --------------------------------- make lists the same size
	set iListName "iGMdirection iGMSineDispAmpl  iTPeriodSine"
	foreach ListName $iListName {
		eval "set theList $$ListName"
		if {[llength $theList]==1} {
			set OneValue $theList
			set theList ""
			foreach SupportNode $iGMdirection {
				lappend theList $OneValue
			}
			set $ListName $theList
		} elseif {[llength $theList]!=[llength $iGMdirection]} {
			puts stderr "ERROR: $ListName and iGMdirection must have the same number of elements"
			return -1
		}
	}

	# ---------------------------------------- create load pattern
	# the following commands are unique to the Multiple-Support Earthquake excitation
	# multiple-support excitation: displacement input at individual nodes
	pattern MultipleSupport $LoadTag  {
		foreach SupportNode $iSupportNode GMdirection $iGMdirection GMSineDispAmpl $iGMSineDispAmpl TPeriodSine $iTPeriodSine DurationSine $iDurationSine {
			set GMseriesTag [expr $LoadTag*200+$SupportNode*4+$GMdirection]
			set DispSeries "Sine 0. $DurationSine $TPeriodSine -factor $GMSineDispAmpl "
			groundMotion $GMseriesTag Plain -disp  $DispSeries  
		    imposedSupportMotion $SupportNode  $GMdirection $GMseriesTag
		};	# end foreach
	};	# end pattern
}