#================================================================
# BISUnits.tcl -- define system of units
# Silvia Mazzoni & Frank McKenna, 2006
# Modified by Yong Li@UCSD, 2010 August 2nd, foxchameleon@gamil.com
#================================================================

set LunitTXT "inch";		# define basic-unit text for output
set FunitTXT "lbf";			# define basic-unit text for output
set TunitTXT "sec";			# define basic-unit text for output

#Basic Units
set in  1.0; 			# define basic units -- Lenth
set lbf 1.0; 			# define basic units -- Force, pound force
set sec 1.0; 			# define basic units -- Time

#Define Constant Parameters
set Inf 1.e10; 			        # a really large number
set Null [expr 1.0/$Inf]; 		# a really small number

# angle
set rad 1.0;
set pi [expr 2*asin($rad)]; 		# define constants
set PI $pi;
set deg [expr $pi/180.0*$rad];

#Length Units
set ft [expr 12.*$in];
set yard [expr 3.0*$ft];
set mile [expr 5280*$ft];

#Area and Volume Units 
set in2 [expr $in*$in]; 		        # inch^2
set in4 [expr $in*$in*$in*$in]; 		# inch^4

#Mass
set g [expr 32.174*$ft/pow($sec,2)]; 	# gravitational acceleration 386.08800(inch/sec^2) = 9.8 (m/sec^2)
set lbs [expr $lbf/$g];

# Force
set kip [expr $lbf*1000];		    # pounds force, here f stands for force

# Pressure (stress) Units or force density
set plf [expr $lbf/pow($ft,1)];         # pounds force per length of foot.
set klf [expr $plf*1000];
set psi [expr $lbf/$in2];              # Pound force per square inch.
set psf [expr $lbf/pow($ft,2)];
set ksi [expr $psi*1000];
set pcf [expr $lbf/pow($ft,3)];			# pounds force per cubic foot

# ###################### Converted Units  ##########################    
#Length Units
set m [expr $in/0.0254];         # meter for length
set cm [expr $m*0.01];
set mm [expr $m*0.001];

#Area and Volume Units 


#Mass, 
set kg   [expr $lbs*2.20462262];
set g    [expr 9.8*$m/$sec/$sec];
set tons [expr 1000.0*$kg];

#Force, 
set N  [expr $kg*$m/pow($sec,2)];
set kN  [expr $N*1000.];

# Pressure (stress) Units and force density
set Pa  [expr $N/$m/$m];
set kPa [expr $kN/$m/$m];
set MPa [expr 1000.*$kPa];
set GPa [expr 1000.*$MPa];

# Units Check
puts "K/M: [expr 1.0*$N/$m/$kg]"
