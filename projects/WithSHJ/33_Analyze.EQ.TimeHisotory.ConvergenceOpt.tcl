# Modified By Yong Li, August 2010, Univerisity of California at San Diego. 
# Contact: foxchameleon@gmail.com / yongli@ucsd.edu
####################################################################################################
#Analyze.EQ.TimeHisotory.ConvergenceOpt.tcl is to improve the convergence capacity

set controlTime [getTime];
set ReducedDTAna [expr $DtAnalysis];

test $testTypeDynamic $TolDynamic $maxNumIterDynamic 1

#Try different tolerences 
set TolReduced1 1.e-4
set TolReduced2 1.e-3
set TolReduced3 1.e-2
set TolReduced4 1.e-1


set icount 0 #IF icount greater than zero, then return to the initial analysis setup paramters
set alFlag 0

#If the analyis failed before finishing the whole history
while {$controlTime < $TmaxAnalysis} {
	set controlTime [getTime]
	puts $controlTime
	
	#If optimal shceme works well then go back to original analysis parameter setup
	if {$icount > 0} {
		test $testTypeDynamic  $TolDynamic $TolDynamic $printFlagDynamic
		set ReducedDTAna [expr $DtAnalysis]
		set icount 0
		if {alFlag == 1} {
		algorithm Newton
		}
	}

    set ok [analyze 1 $ReducedDTAna] #If next step works well then no need to try!

	#Try to find a optimal way to avoid the convergence problem
	if {$ok != 0} {
		puts "Trying Newton with reduced tolerance .."
		test $testTypeDynamic  $TolReduced1 $TolDynamic $printFlagDynamic
		set ReducedDTAna [expr $DtAnalysis]
		set ok [analyze 1 $ReducedDTAna]
		set icount [expr $icount+1]
	}
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/2 and reduced1 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/2.]
		test $testTypeDynamic  $TolReduced1 50 2
		set ok [analyze 2 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/2 and reduced3 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/2.]
		test $testTypeDynamic $TolReduced3 50 2
		set ok [analyze 2 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/2 and reduced4 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/2.]
		test $testTypeDynamic  $TolReduced4 50 2
		set ok [analyze 10 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/10 and reduced4 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/50.]
		test $testTypeDynamic  $TolReduced4 50 2
		set ok [analyze 50 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/10 and reduced4 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/50.]
		test $testTypeDynamic  $TolReduced4 50 2
		algorithm NewtonLineSearch 0.5; set alFlag 1;
		set ok [analyze 50 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	if {$ok != 0} {
		puts "oooppppsss...The structural analysis is a total disaster!"
		break
		set ok 1
	};      # end if ok !0
};


#Output the analyze results for users to know: successful or not.
set IDctrlNode 1;
set IDctrlDOF 1;
set fmt1 "%s Earthquake analysis: CtrlNode %.3i, dof %.1i, Disp=%.4f %s";	# format for screen/file output of DONE/PROBLEM analysis

if {$ok != 0 } {
puts [format $fmt1 "PROBLEM" $IDctrlNode $IDctrlDOF [nodeDisp $IDctrlNode $IDctrlDOF]]
} else {
	puts [format $fmt1 "DONE"  $IDctrlNode $IDctrlDOF [nodeDisp $IDctrlNode $IDctrlDOF]]
}
puts "Ground Motion Done. End Time: [getTime] out of $TmaxAnalysis "
puts " ---------------Done Earthquake Analysis -----------------"