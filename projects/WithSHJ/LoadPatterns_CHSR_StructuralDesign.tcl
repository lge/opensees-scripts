# This file is to define the load patterns of combinations for design of Structures in CHSR Project
# Developed by Yong Li, University of California, San Diego
# Developed Date: April 10th, 2012
# Contact: foxchameleon@gmail.com, yongli@ucsd.edu
# Modified: 
#         1. Corrected the gravity loading direction. 2012.05.02
#         2. The gravity results from direct loading is not correct, corrected on 2012.05.03
#         3. Apply LF on the rail considering the eccentricity.
# Permanent Loads:
# DC, DW, DD, EV, EH, ES, SE, EL, PS, CR, SH, WA

# Transient Loads:
# LLP, LLV, LLRR, LLH, LLS, I, LF, NE, CF, DR, CL, WS, WL, SS, TU, TG, FR, MCE, OBE, WAD, ED

# Load Combinations:
# Strength 1, Strength 2, Strength 3, Strength 4, Strength 5, Extreme 1, Extreme 2, Extreme 3, Service 1, Service 2,Service 3,...


#########################################################################################
############################# DC & DW (Dead Load from Structures) #######################
#########################################################################################
# The following defines the load pattern
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________
# Static Gravity Pattern.
if {$gamma_1 > 0} {
	set ApplyGrvityOption "viaDirectLoad";
	# set ApplyGrvityOption "viaConstantG";
	# set gamma_1 1.25;
	if {$ApplyGrvityOption == "viaDirectLoad"} {
		set gravityLoadPatternTag 1;
		timeSeries Linear 1 -factor $gamma_1;
		pattern Plain 1  1  {
		# _______________________________________________________________________________________________________
		# _______________________________________________________________________________________________________
		# Define the load for the piers
			for {set pierTag 1} {$pierTag <= $numSpan-1} {incr pierTag } {
						# Define the load for the pile cap node.
					load [expr $pierTag*100+1] 0 0 [expr -$g*$pileCapLineMass*0.5*$pileCapDepth] 0 0 0;
						# Start to define the mass of nodes above the foundation pile cap.
					set i 1; 
					set ne_pier [expr max(round(($pierHeightArray($pierTag)-$pierHeadHeight)/$pierEleLength),1)];
					while {$i<=$ne_pier+1} {
						# For the last element which may have different length from other elements
						if {$i == 1} {
							set tempMass [expr $pileCapLineMass*0.25*$pileCapDepth+$pierLineMass*0.5*$pierEleLength];
							load [expr $pierTag*100+1+$i] 0 0 [expr -$g*$tempMass] 0 0 0;
						} elseif {$i == $ne_pier + 1 } {
							set lengthLastEle [expr ($pierHeightArray($pierTag)-$pierHeadHeight)-($ne_pier-1)*$pierEleLength];
							set tempMass      [expr $pierLineMass*0.5*$lengthLastEle + $pierHeadLineMass*$pierHeadHeight];
							load [expr $pierTag*100+1+$i] 0 0 [expr -$g*$tempMass] 0 0 0;
						} elseif {$i == $ne_pier } {
							set lengthLastEle [expr ($pierHeightArray($pierTag)-$pierHeadHeight)-($ne_pier-1)*$pierEleLength];
							set tempMass      [expr $pierLineMass*0.5*$pierEleLength+$pierLineMass*0.5*$lengthLastEle];
							load [expr $pierTag*100+1+$i] 0 0 [expr -$g*$tempMass] 0 0 0;
						} else {
							load [expr $pierTag*100+1+$i] 0 0 [expr -$g*$pierLineMass*1.0*$pierEleLength] 0 0 0;
						}
						set i [expr $i+1];
					};
					unset i;
					# Start to define the mass of nodes for isolators
				if {[expr $pierTag%$numSpanContinuous]!= 0 || $pierTag== 0 || $pierTag ==$numSpan} {	 
					# Define the nodes below the isolator
					load [expr $pierTag*100+25] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+26] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					# Define the nodes above the isolator
					load [expr $pierTag*100+35] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+36] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
				} else {
					# Define the nodes below the isolator
					load [expr $pierTag*100+21] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+22] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+23] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+24] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					# Define the nodes above the isolator
					load [expr $pierTag*100+31] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+32] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+33] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
					load [expr $pierTag*100+34] 0 0 [expr -$g*0.5*$isolatorMass] 0 0 0 ;
				}

			}; # end of while loop for different piers.
			unset pierTag;	
				
			# Define the mass of nodes between the piers
			for {set spanTag 1} {$spanTag <= $numSpan } {incr spanTag } {
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________
				# Define the mass of nodes for the deck
				# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
				if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
					# The case, when left end is not around an expansion joint.
						set pierTag [expr $spanTag-1]; # Left pier of a certain span.
						set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
					load [expr $spanTag*10000+1] 0 0 [expr -$g*$tempMass] 0 0 0;
						set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+ ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength];
					load [expr $spanTag*10000+2] 0 0 [expr -$g*$tempMass] 0 0 0;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEndTransitionLength];
					load [expr $spanTag*10000+3] 0 0 [expr -$g*$tempMass] 0 0 0;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEleLength];
					load [expr $spanTag*10000+4] 0 0 [expr -$g*$tempMass] 0 0 0;
					set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
				} else {
					# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
						set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x];
					load [expr $spanTag*10000+1] 0 0 [expr -$g*$tempMass] 0 0 0;
						set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x + ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength];
					load [expr $spanTag*10000+2] 0 0 [expr -$g*$tempMass] 0 0 0;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEndTransitionLength];
					load [expr $spanTag*10000+3] 0 0 [expr -$g*$tempMass] 0 0 0;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEleLength];
					load [expr $spanTag*10000+4] 0 0 [expr -$g*$tempMass] 0 0 0;
					set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
				};
					

					# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
					set i 1; 
					set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
					while {$i<=$ne_deckMiddle} {
						# set x_temp [expr $x_temp + $deckEleLength];
						# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
						# For the last element which may have different length from other elements
						if {$i == $ne_deckMiddle} {
								set tempMass [expr ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength];
							load [expr $spanTag*10000+6]  0 0 [expr -$g*$tempMass] 0 0 0;
						} elseif {$i == $ne_deckMiddle-1} {
								set tempMass [expr ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEleLength + ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
							load [expr $spanTag*10000+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0;
						} else {
								set tempMass [expr ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*1.0*$deckEleLength];
							load [expr $spanTag*10000+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0;
						}
						set i [expr $i+1];
					};
					# set tempNodeTag [expr $spanTag*10000+10+$i];
					# set x_temp $x_tempLast;
					# unset x_tempLast;
					# unset x_temp;
					unset i;
					
					# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
				if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
					# The case, when right end is not around an expansion joint.
					set pierTag [expr $spanTag]; # Right pier of a certain span.
					#mass [expr $spanTag*10000+6]  $tempMass $tempMass $tempMass 0 0 0;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*1.0*$deckEndTransitionLength];
					load [expr $spanTag*10000+7] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEndDiaphragmLength];
					load [expr $spanTag*10000+8] 0 0 [expr -$g*$tempMass] 0 0 0 ;
					if {$spanTag== $numSpan} {
						set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*1.0*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+9] 0 0 [expr -$g*$tempMass] 0 0 0 ;
					}; # For the right abutment of the last span	
				} else {
					# The case, when right end is around an expansion joint.
					set pierTag [expr $spanTag]; # Right pier of a certain span.
					#mass [expr $spanTag*10000+6]  $tempMass $tempMass $tempMass 0 0 0;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*1.0*$deckEndTransitionLength];
					load [expr $spanTag*10000+7] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x];
					load [expr $spanTag*10000+8] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x];
					load [expr $spanTag*10000+9] 0 0 [expr -$g*$tempMass] 0 0 0 ;
				}
			 if {$includeTrack == "YES"} {	
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________
				# Define the load of nodes above the deck for the two line of bottom nodes of fastener.
				for {set iTrack 1} {$iTrack <= 2} {incr iTrack} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
							set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr (1-$bmr)*$ballastLineMass*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*$iTrack+1] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+(1-$bmr)*$ballastLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+2] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+3] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*0.5*$deckEndTransitionLength+(1-$bmr)*$ballastLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*$iTrack+4] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*$iTrack+1] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*$isolatorLoc_x + (1-$bmr)*$ballastLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+2] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+3] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*0.5*$deckEndTransitionLength+(1-$bmr)*$ballastLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*$iTrack+4] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];			
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+(1-$bmr)*$ballastLineMass*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*$iTrack+6]  0 0 [expr -$g*$tempMass] 0 0 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*$deckEleLength + (1-$bmr)*$ballastLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*$iTrack+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							} else {
									set tempMass [expr (1-$bmr)*$ballastLineMass*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*$iTrack+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*$iTrack+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*$iTrack+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+7] 0 0 [expr -$g*$tempMass] 0 0 0; 
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*0.5*$deckEndTransitionLength+(1-$bmr)*$ballastLineMass*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*$iTrack+8] 0 0 [expr -$g*$tempMass] 0 0 0;
						if {$spanTag== $numSpan} {
							set tempMass [expr (1-$bmr)*$ballastLineMass*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*$iTrack+9] 0 0 [expr -$g*$tempMass] 0 0 0;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*$iTrack+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+7] 0 0 [expr -$g*$tempMass] 0 0 0;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*0.5*$deckEndTransitionLength+(1-$bmr)*$ballastLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*$iTrack+8] 0 0 [expr -$g*$tempMass] 0 0 0;
							set tempMass [expr (1-$bmr)*$ballastLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*$iTrack+9] 0 0 [expr -$g*$tempMass] 0 0 0;		
					}
				}
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________ 
				# Define the nodes above the deck for the two line of top nodes of rails for track 1 on right.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
							set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+4)+1] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+1] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x + $railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] 0 0 [expr -$g*$tempMass] 0 0 0 ;	
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempMass [expr $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+4)+6]  0 0 [expr -$g*$tempMass] 0 0 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempMass [expr $railLineMass*0.5*$deckEleLength + $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							} else {
									set tempMass [expr $railLineMass*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+4)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+4)+8] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						if {$spanTag== $numSpan} {
							set tempMass [expr $railLineMass*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+4)+9] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+8] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+9] 0 0 [expr -$g*$tempMass] 0 0 0 ;		
					}
				}		
					
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________	
				# Define the nodes above the deck for the two line of top nodes of rails for track 2 on left.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
							set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+6)+1] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+1] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x + $railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempMass [expr $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+6)+6]  0 0 [expr -$g*$tempMass] 0 0 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempMass [expr $railLineMass*0.5*$deckEleLength + $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							} else {
									set tempMass [expr $railLineMass*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+6)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodal load on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+6)+8] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						if {$spanTag== $numSpan} {
							set tempMass [expr $railLineMass*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+6)+9] 0 0 [expr -$g*$tempMass] 0 0 0 ;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+8] 0 0 [expr -$g*$tempMass] 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+9] 0 0 [expr -$g*$tempMass] 0 0 0 ;	
					}
				}	

			}
		}
		}
	} elseif {$ApplyGrvityOption == "viaConstantG"} {
		set gravityLoadPatternTag 1;
		timeSeries Constant 1 -factor [expr $g];
		pattern UniformExcitation $gravityLoadPatternTag 3 -accel 1;
	} else {
		puts "YONG: No Gravity Load Defined!"
	};
};


# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________
# Define load pattern for High-speed train live loads (LLV)
# To compute the linear load along the bridge
# Trainsets Type 3;
if {$gamma_2 > 0} {

	#########################################################################################
	############################# LLV/LLRR (Vertical and Longitudinal)#######################
	#########################################################################################

	# Define the load patterns
	# set gamma_2 1.75;
	# set LLVyesOrno_1    1.0;
	# set LLRRyesOrno_1   0.0;
	# set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	# set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	# set leftLoc_1       [expr max($startingLoc_1,0)];
	# set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	# set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	# set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	# set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	# set LLVyesOrno_2    1.0;
	# set LLRRyesOrno_2   0.0;
	# set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	# set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	# set leftLoc_2       [expr max($startingLoc_2,0)];
	# set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	# set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	# set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	# set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	timeSeries Linear 2 -factor $gamma_2;
	pattern Plain 2  2 {
	# _______________________________________________________________________________________________________
	# _______________________________________________________________________________________________________
		# Define the LLV live loads on the bridge, together with the braking and traction force
		# Vertical Loads.
		set tempLoadLineDensityZ1   [expr $LLVyesOrno_1*$LLVLineDensity*$LLVimpactFactor+$LLRRyesOrno_1*$LLRRLineDensity*$LLRRimpactFactor];
		set tempLoadLineDensityZ2   [expr $LLVyesOrno_2*$LLVLineDensity*$LLVimpactFactor+$LLRRyesOrno_2*$LLRRLineDensity*$LLRRimpactFactor];	
		
		# Horizontal Loads.
			if {$tractionBrake_1>=0} {
				set tempLineForceX1  [expr $LLVyesOrno_1*$LLVLineBraking + $LLRRyesOrno_1*$LLRRLineBraking];
				set tempLineMomentY1 [expr $LLVyesOrno_1*$LLVLineBraking*$LLVBrakeZ_ROT + $LLRRyesOrno_1*$LLRRLineBraking*$LLRRBrakeZ_ROT];
			} else {
				set tempLineForceX1  [expr $LLVyesOrno_1*$LLVLineTraction + $LLRRyesOrno_1*$LLRRLineTraction];
				set tempLineMomentY1 [expr $LLVyesOrno_1*$LLVLineTraction*$LLVTractionZ_ROT +$LLRRyesOrno_1*$LLRRLineTraction*$LLRRTractionZ_ROT];
			};
			
			if {$tractionBrake_2>=0} {
				set tempLineForceX2 [expr $LLVyesOrno_2*$LLVLineBraking + $LLRRyesOrno_2*$LLRRLineBraking];
				set tempLineMomentY2 [expr $LLVyesOrno_2*$LLVLineBraking*$LLVBrakeZ_ROT + $LLRRyesOrno_2*$LLRRLineBraking*$LLRRBrakeZ_ROT];
			} else {
				set tempLineForceX2 [expr $LLVyesOrno_2*$LLVLineTraction + $LLRRyesOrno_2*$LLRRLineTraction];
				set tempLineMomentY2 [expr $LLVyesOrno_2*$LLVLineTraction*$LLVTractionZ_ROT + $LLRRyesOrno_2*$LLRRLineTraction*$LLRRTractionZ_ROT];
			};
			
		set tempLoadLineDensityX1 [expr $direction_1*$tractionBrake_1*$tempLineForceX1];
		set tempLoadLineDensityX2 [expr $direction_2*$tractionBrake_2*$tempLineForceX2];
		set tempLoadLineDensityYY1 [expr $direction_1*$tractionBrake_1*$tempLineMomentY1];
		set tempLoadLineDensityYY2 [expr $direction_2*$tractionBrake_2*$tempLineMomentY2];	
		
		
		for {set spanTag 1} {$spanTag <= $numSpan } {incr spanTag } {
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________
			 if {$includeTrack == "YES"} {	
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________ 
				# Define the nodes above the deck for the two line of top nodes of rails for track 1 on right.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
						set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
							set tempLoadZ  [expr 0.5*$flag*$tempLoadLineDensityZ1*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
							set tempLoadX  [expr 0.5*$flag*$tempLoadLineDensityX1*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+4)+1] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*$isolatorLoc_x];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+1] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;	
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};					
									set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
									set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
									set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
									set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
									set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
									set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							} else {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};					
									set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*1.0*$deckEleLength];
									set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*1.0*$deckEleLength];
									set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+4)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*1.0*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*1.0*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndDiaphragmLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndDiaphragmLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+4)+8] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
						if {$spanTag== $numSpan} {
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*1.0*$deckEndDiaphragmLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*1.0*$deckEndDiaphragmLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+4)+9] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*1.0*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*1.0*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+8] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX1*0.5*$isolatorLoc_x];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+9] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;		
					}
				}		
					
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________	
				# Define the nodes above the deck for the two line of top nodes of rails for track 2 on left.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
						set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};					
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+6)+1] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*$isolatorLoc_x];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+1] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};						
									set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
									set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
									set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};
									set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
									set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
									set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							} else {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};							
									set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*1.0*$deckEleLength];
									set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*1.0*$deckEleLength];
									set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+6)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodal load on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*1.0*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*1.0*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndDiaphragmLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndDiaphragmLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+6)+8] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
						if {$spanTag== $numSpan} {
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*1.0*$deckEndDiaphragmLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*1.0*$deckEndDiaphragmLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+6)+9] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*1.0*$deckEndTransitionLength];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*1.0*$deckEndTransitionLength];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+8] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadZ [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
							set tempLoadX [expr 0.5*$flag*$tempLoadLineDensityX2*0.5*$isolatorLoc_x];
							set tempLoadYY [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+9] $tempLoadX 0 [expr -$tempLoadZ] 0 $tempLoadYY 0 ;	
					};
				};	
			};
		};
	};
}; # End of if gamma_2 > 0;

#########################################################################################
################################## WS (Wind Load on Structure)###########################
#########################################################################################
# Wind Loads on Structures (WS)
if {$gamma_4 > 0 }  {
	# set gamma_4 1.40;
	timeSeries Linear 4 -factor $gamma_4;
	pattern Plain 4  4 {
	# _______________________________________________________________________________________________________
	# _______________________________________________________________________________________________________
		# Define wind the load for the piers
		set windLineDensity [expr $windDirection*$columnWindPressure*$columnDiameter];
		for {set pierTag 1} {$pierTag <= $numSpan-1} {incr pierTag } {
					# Define the wind load for the pile cap node.
				load [expr $pierTag*100+1] 0 0 [expr $windLineDensity*0.5*$pileCapDepth] 0 0 0;
					# Start to define the mass of nodes above the foundation pile cap.
				set i 1; 
				set ne_pier [expr max(round(($pierHeightArray($pierTag)-$pierHeadHeight)/$pierEleLength),1)];
				while {$i<=$ne_pier+1} {
					# For the last element which may have different length from other elements
					if {$i == 1} {
						set tempWind [expr $windLineDensity*0.25*$pileCapDepth+$windLineDensity*0.5*$pierEleLength];
						load [expr $pierTag*100+1+$i] 0 [expr $tempWind] 0 0 0 0;
					} elseif {$i == $ne_pier + 1 } {
						set lengthLastEle [expr ($pierHeightArray($pierTag)-$pierHeadHeight)-($ne_pier-1)*$pierEleLength];
						set tempWind      [expr $windLineDensity*0.5*$lengthLastEle + $windLineDensity*$pierHeadHeight];
						load [expr $pierTag*100+1+$i] 0 [expr $tempWind] 0 0 0 0;
					} elseif {$i == $ne_pier } {
						set lengthLastEle [expr ($pierHeightArray($pierTag)-$pierHeadHeight)-($ne_pier-1)*$pierEleLength];
						set tempWind      [expr $windLineDensity*0.5*$pierEleLength+$windLineDensity*0.5*$lengthLastEle];
						load [expr $pierTag*100+1+$i] 0 [expr $tempWind] 0 0 0 0;
					} else {
						load [expr $pierTag*100+1+$i] 0 [expr $windLineDensity*1.0*$pierEleLength] 0 0 0 0;
					}
					set i [expr $i+1];
				};
				unset i;

		}; # end of while loop for different piers.
		unset pierTag;	
				
		# Define the mass of nodes between the piers
		for {set spanTag 1} {$spanTag <= $numSpan } {incr spanTag } {
			# _______________________________________________________________________________________________________
			# _______________________________________________________________________________________________________
			# Define the mass of nodes for the deck
			# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
			set windLineDensity [expr $windDirection*$deckWindPressure*($H_deck + $deckWindHeightExtra)];
			if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
				# The case, when left end is not around an expansion joint.
					set pierTag [expr $spanTag-1]; # Left pier of a certain span.
					set tempWind [expr $windLineDensity*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
				load [expr $spanTag*10000+1] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+ $windLineDensity*0.5*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+2] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+3] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*0.5*$deckEndTransitionLength+ $windLineDensity*0.5*$deckEleLength];
				load [expr $spanTag*10000+4] 0 [expr $tempWind] 0 0 0 0;
				set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
			} else {
				# The case, when left end is indeed around an expansion joint.
					set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
					set tempWind [expr $windLineDensity*0.5*$isolatorLoc_x];
				load [expr $spanTag*10000+1] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*$isolatorLoc_x + $windLineDensity*0.5*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+2] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+3] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*0.5*$deckEndTransitionLength+ $windLineDensity*0.5*$deckEleLength];
				load [expr $spanTag*10000+4] 0 [expr $tempWind] 0 0 0 0;
				set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
			};
				

				# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
				set i 1; 
				set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
				while {$i<=$ne_deckMiddle} {
					# set x_temp [expr $x_temp + $deckEleLength];
					# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
					# For the last element which may have different length from other elements
					if {$i == $ne_deckMiddle} {
							set tempWind [expr $windLineDensity*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+ $windLineDensity*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+6]  0 [expr $tempWind] 0 0 0 0;
					} elseif {$i == $ne_deckMiddle-1} {
							set tempWind [expr $windLineDensity*0.5*$deckEleLength + $windLineDensity*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
						load [expr $spanTag*10000+10+$i] 0 [expr $tempWind] 0 0 0 0;
					} else {
							set tempWind [expr $windLineDensity*1.0*$deckEleLength];
						load [expr $spanTag*10000+10+$i] 0 [expr $tempWind] 0 0 0 0;
					}
					set i [expr $i+1];
				};
				# set tempNodeTag [expr $spanTag*10000+10+$i];
				# set x_temp $x_tempLast;
				# unset x_tempLast;
				# unset x_temp;
				unset i;
				
				# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
			if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
				# The case, when right end is not around an expansion joint.
				set pierTag [expr $spanTag]; # Right pier of a certain span.
				#mass [expr $spanTag*10000+6]  $tempWind $tempWind $tempWind 0 0 0;
					set tempWind [expr $windLineDensity*0.5*1.0*$deckEndTransitionLength];
				load [expr $spanTag*10000+7] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*0.5*$deckEndTransitionLength+ $windLineDensity*0.5*$deckEndDiaphragmLength];
				load [expr $spanTag*10000+8] 0 [expr $tempWind] 0 0 0 0;
				if {$spanTag== $numSpan} {
					set tempWind [expr $windLineDensity*1.0*$deckEndDiaphragmLength];
					load [expr $spanTag*10000+9] 0 [expr $tempWind] 0 0 0 0;
				}; # For the right abutment of the last span	
			} else {
				# The case, when right end is around an expansion joint.
				set pierTag [expr $spanTag]; # Right pier of a certain span.
				#mass [expr $spanTag*10000+6]  $tempWind $tempWind $tempWind 0 0 0;
					set tempWind [expr $windLineDensity*0.5*1.0*$deckEndTransitionLength];
				load [expr $spanTag*10000+7] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*0.5*$deckEndTransitionLength+ ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x];
				load [expr $spanTag*10000+8] 0 [expr $tempWind] 0 0 0 0;
					set tempWind [expr $windLineDensity*0.5*$isolatorLoc_x];
				load [expr $spanTag*10000+9] 0 [expr $tempWind] 0 0 0 0;
			}
		}
	}
};  # End of if gamma_4 > 0;

#########################################################################################
################################## WL (Wind Load on Vehcile)#############################
#########################################################################################
# Define the wind load on vehcile
if {$gamma_5 > 0 } {
	# set gamma_5 1.0;
	timeSeries Linear 5 -factor $gamma_5;
	pattern Plain 5  5 {
	# _______________________________________________________________________________________________________
	# _______________________________________________________________________________________________________
		# Define the LLV live loads on the bridge, together with the braking and traction force
		# Vertical Loads.
		set tempLoadLineDensityY  [expr $windDirection*$reducedFactor*$trainWindPressure*$trainHeight];
		set tempLoadLineDensityXX [expr -$windDirection*$reducedFactor*$trainWindPressure*$trainHeight*$trainWindLocTOR];	
		
		for {set spanTag 1} {$spanTag <= $numSpan } {incr spanTag } {
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________
			 if {$includeTrack == "YES"} {	
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________ 
				# Define the nodes above the deck for the two line of top nodes of rails for track 1 on right.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
						set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
							set tempLoadY  [expr 0.5*$flag*$tempLoadLineDensityZ1*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+4)+1] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+1] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;	
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};					
									set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
									set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+4)+6]  0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
									set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
									set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ1*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							} else {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};					
									set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*1.0*$deckEleLength];
									set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+4)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*1.0*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndDiaphragmLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+4)+8] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
						if {$spanTag== $numSpan} {
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*1.0*$deckEndDiaphragmLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+4)+9] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*1.0*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+8] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $leftLoc_1 && $tempNodeLocX <= $rightLoc_1} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ1*0.5*$isolatorLoc_x];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY1*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+9] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;		
					}
				}		
				
				
				# Caution, the windload on the live loads can only happen when train is on bridge, on can not be applied to
				# both trains if there is overlap.
				set temLeftLoc_2 [expr max($rightLoc_1,$leftLoc_2)];
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________	
				# Define the nodes above the deck for the two line of top nodes of rails for track 2 on left.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
						set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};					
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+6)+1] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+1] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$isolatorLoc_x + 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};						
									set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
									set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+6)+6]  0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};
									set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
									set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$deckEleLength + 0.5*$flag*$tempLoadLineDensityZ2*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							} else {
									set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
									if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};							
									set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*1.0*$deckEleLength];
									set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+6)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodal load on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*1.0*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndDiaphragmLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+6)+8] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
						if {$spanTag== $numSpan} {
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*1.0*$deckEndDiaphragmLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+6)+9] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempLoad $tempLoad $tempLoad 0 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*1.0*$deckEndTransitionLength];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*0.5*$deckEndTransitionLength+0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+8] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;
							set tempNodeLocX [nodeCoord [expr $spanTag*10000+1000*($iRail+4)+1] 1];
							if {$tempNodeLocX >= $temLeftLoc_2 && $tempNodeLocX <= $rightLoc_2} {set flag 1.0} else {set flag 0.0};				
							set tempLoadY [expr 0.5*$flag*$tempLoadLineDensityZ2*0.5*$isolatorLoc_x];
							set tempLoadXX [expr 0.5*$flag*$tempLoadLineDensityYY2*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+9] 0 [expr $tempLoadY] 0 $tempLoadXX 0 0;	
					};
				};	
			};
		};
	};
}; # End of if gamma_5 > 0;

#########################################################################################
############################# EQ (Transversal and Longitudinal)   #######################
#########################################################################################

if {$gamma_11 > 0} {
	source designUHSSa_T.tcl; # Define T1_y, T1_x,OBESa_y and OBESa_x,MCESa_y and MCESa_x
	set EQAnalysisMethod "ESA";
	# set EQHazardLevel "OBE";

	# set gamma_11 1.0;
	# set yxCombinationCase "Y";
	# set EQDirection_x 1; # 1 0r -1;
	# set EQDirection_y 1; # 1 0r -1;

	# To compute the amplification factor C based on TM11.7.3.15 and TM11.7.3.16 to account for the uncertanity
	# associated with calculation of structural period for stiff structures.
	set T0Peak_y [expr 0.2];
	set T0Peak_x [expr 0.2];
	if { $EQHazardLevel == "MCE"} {
		if {$T1_y < $T0Peak_y} {
			set C_y [expr 0.8/($T1_y/$T0Peak_y)];
		} else {
			set C_y 1.0;
		};
		
		if {$T1_x < $T0Peak_x} {
			set C_x [expr 0.8/($T1_x/$T0Peak_x)];
		} else {
			set C_x 1.0;
		};
		
		set tempSa_x  $MCESa_x;
		set tempSa_y  $MCESa_y;
	} else {
		set C_y 1.0;
		set C_x 1.0;
		set tempSa_x  $OBESa_x;
		set tempSa_y  $OBESa_y;
	};

	# Set Longitudinal and Transveral combination coefficient;
	if {$yxCombinationCase == "X"} {
		set yCombFactor 0.3;
		set xCombFactor 1.0;
	} elseif {$yxCombinationCase == "Y"} {
		set yCombFactor 1.0;
		set xCombFactor 0.3;
	} else {
		puts "YONG: THE Earthquake Demand 100 percent-30 percent rule for longitudinal and transversal does not work!";
	}

	# Set the Earthquake MASS considering trains or not for OBE or MCE events
	if { $EQHazardLevel == "MCE"} {
		set includeTrainMassTrack1 0.0;
		set includeTrainMassTrack2 0.0;
		set trainLineMassTrack1 [expr $includeTrainMassTrack1*($LLVLineMass*$LLVyesOrno_1+$LLRRLineMass*$LLRRyesOrno_1)];
		set trainLineMassTrack2 [expr $includeTrainMassTrack2*($LLVLineMass*$LLVyesOrno_2+$LLRRLineMass*$LLRRyesOrno_2)];
	} elseif {$EQHazardLevel == "OBE"} {
		set includeTrainMassTrack1 0.5;
		set includeTrainMassTrack2 0.5;
		set trainLineMassTrack1 [expr $includeTrainMassTrack1*($LLVLineMass*$LLVyesOrno_1+$LLRRLineMass*$LLRRyesOrno_1)];
		set trainLineMassTrack2 [expr $includeTrainMassTrack2*($LLVLineMass*$LLVyesOrno_2+$LLRRLineMass*$LLRRyesOrno_2)];
	};

	# _______________________________________________________________________________________________________
	# _______________________________________________________________________________________________________
	set EQLoadPatternTag 11;
	timeSeries Linear 11 -factor $gamma_11;
	pattern Plain 11  11  {
	# _______________________________________________________________________________________________________
	# _______________________________________________________________________________________________________
	# Define the load for the piers
		set tempSa_y [expr $EQDirection_y*$yCombFactor*$C_y*$tempSa_y];
		set tempSa_x [expr $EQDirection_x*$xCombFactor*$C_x*$tempSa_x];
		for {set pierTag 1} {$pierTag <= $numSpan-1} {incr pierTag } {
					# Define the load for the pile cap node.
				load [expr $pierTag*100+1] [expr $tempSa_x*$pileCapLineMass*0.5*$pileCapDepth] [expr $tempSa_y*$pileCapLineMass*0.5*$pileCapDepth] 0 0 0 0;
					# Start to define the mass of nodes above the foundation pile cap.
				set i 1; 
				set ne_pier [expr max(round(($pierHeightArray($pierTag)-$pierHeadHeight)/$pierEleLength),1)];
				while {$i<=$ne_pier+1} {
					# For the last element which may have different length from other elements
					if {$i == 1} {
						set tempMass [expr $pileCapLineMass*0.25*$pileCapDepth+$pierLineMass*0.5*$pierEleLength];
						load [expr $pierTag*100+1+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					} elseif {$i == $ne_pier + 1 } {
						set lengthLastEle [expr ($pierHeightArray($pierTag)-$pierHeadHeight)-($ne_pier-1)*$pierEleLength];
						set tempMass      [expr $pierLineMass*0.5*$lengthLastEle + $pierHeadLineMass*$pierHeadHeight];
						load [expr $pierTag*100+1+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					} elseif {$i == $ne_pier } {
						set lengthLastEle [expr ($pierHeightArray($pierTag)-$pierHeadHeight)-($ne_pier-1)*$pierEleLength];
						set tempMass      [expr $pierLineMass*0.5*$pierEleLength+$pierLineMass*0.5*$lengthLastEle];
						load [expr $pierTag*100+1+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					} else {
						load [expr $pierTag*100+1+$i] [expr $tempSa_x*$pierLineMass*1.0*$pierEleLength] [expr $tempSa_y*$pierLineMass*1.0*$pierEleLength] 0 0 0 0;
					}
					set i [expr $i+1];
				};
				unset i;
				# Start to define the mass of nodes for isolators
			if {[expr $pierTag%$numSpanContinuous]!= 0 || $pierTag== 0 || $pierTag ==$numSpan} {	 
				# Define the nodes below the isolator
				load [expr $pierTag*100+25] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+26] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				# Define the nodes above the isolator
				load [expr $pierTag*100+35] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+36] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
			} else {
				# Define the nodes below the isolator
				load [expr $pierTag*100+21] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+22] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+23] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+24] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				# Define the nodes above the isolator
				load [expr $pierTag*100+31] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+32] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+33] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
				load [expr $pierTag*100+34] [expr $tempSa_x*0.5*$isolatorMass] [expr $tempSa_x*0.5*$isolatorMass] 0 0 0 0 ;
			}

		}; # end of while loop for different piers.
		unset pierTag;	
			
		# Define the mass of nodes between the piers
		for {set spanTag 1} {$spanTag <= $numSpan } {incr spanTag } {
			# _______________________________________________________________________________________________________
			# _______________________________________________________________________________________________________
			# Define the mass of nodes for the deck
			# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
			if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
				# The case, when left end is not around an expansion joint.
					set pierTag [expr $spanTag-1]; # Left pier of a certain span.
					set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
				load [expr $spanTag*10000+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+ ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEleLength];
				load [expr $spanTag*10000+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
				set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
			} else {
				# The case, when left end is indeed around an expansion joint.
					set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
					set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x];
				load [expr $spanTag*10000+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x + ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEndTransitionLength];
				load [expr $spanTag*10000+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEleLength];
				load [expr $spanTag*10000+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
				set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
			};
				

				# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
				set i 1; 
				set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
				while {$i<=$ne_deckMiddle} {
					# set x_temp [expr $x_temp + $deckEleLength];
					# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
					# For the last element which may have different length from other elements
					if {$i == $ne_deckMiddle} {
							set tempMass [expr ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+6]  [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					} elseif {$i == $ne_deckMiddle-1} {
							set tempMass [expr ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEleLength + ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
						load [expr $spanTag*10000+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					} else {
							set tempMass [expr ($deckMiddleLineMass+2*$bmr*$ballastLineMass)*1.0*$deckEleLength];
						load [expr $spanTag*10000+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
					}
					set i [expr $i+1];
				};
				# set tempNodeTag [expr $spanTag*10000+10+$i];
				# set x_temp $x_tempLast;
				# unset x_tempLast;
				# unset x_temp;
				unset i;
				
				# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
			if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
				# The case, when right end is not around an expansion joint.
				set pierTag [expr $spanTag]; # Right pier of a certain span.
				#mass [expr $spanTag*10000+6]  $tempMass $tempMass $tempMass 0 0 0;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*1.0*$deckEndTransitionLength];
				load [expr $spanTag*10000+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$deckEndDiaphragmLength];
				load [expr $spanTag*10000+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
				if {$spanTag== $numSpan} {
					set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*1.0*$deckEndDiaphragmLength];
					load [expr $spanTag*10000+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
				}; # For the right abutment of the last span	
			} else {
				# The case, when right end is around an expansion joint.
				set pierTag [expr $spanTag]; # Right pier of a certain span.
				#mass [expr $spanTag*10000+6]  $tempMass $tempMass $tempMass 0 0 0;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*1.0*$deckEndTransitionLength];
				load [expr $spanTag*10000+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
					set tempMass [expr ($deckEndTransitionLineMass+2*$bmr*$ballastLineMass)*0.5*0.5*$deckEndTransitionLength+ ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x];
				load [expr $spanTag*10000+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
					set tempMass [expr ($deckEndDiaphragmLineMass+2*$bmr*$ballastLineMass)*0.5*$isolatorLoc_x];
				load [expr $spanTag*10000+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
			}
			 if {$includeTrack == "YES"} {	
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________
				# Define the load of nodes above the deck for the two line of bottom nodes of fastener.
				for {set iTrack 1} {$iTrack <= 2} {incr iTrack} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {$iTrack == 1} {
						set trainLineMass $trainLineMassTrack1;
						set totalTrackLineMass [expr $trainLineMass + (1-$bmr)*$ballastLineMass];
					} else {
						set trainLineMass $trainLineMassTrack2;
						set totalTrackLineMass [expr $trainLineMass + (1-$bmr)*$ballastLineMass];
					};
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
							set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $totalTrackLineMass*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*$iTrack+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $totalTrackLineMass*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+$totalTrackLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $totalTrackLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $totalTrackLineMass*0.5*0.5*$deckEndTransitionLength+$totalTrackLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*$iTrack+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $totalTrackLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*$iTrack+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $totalTrackLineMass*0.5*$isolatorLoc_x + $totalTrackLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $totalTrackLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $totalTrackLineMass*0.5*0.5*$deckEndTransitionLength+$totalTrackLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*$iTrack+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];			
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempMass [expr $totalTrackLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+$totalTrackLineMass*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*$iTrack+6]  [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempMass [expr $totalTrackLineMass*0.5*$deckEleLength + $totalTrackLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*$iTrack+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							} else {
									set tempMass [expr $totalTrackLineMass*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*$iTrack+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*$iTrack+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*$iTrack+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $totalTrackLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0; 
							set tempMass [expr $totalTrackLineMass*0.5*0.5*$deckEndTransitionLength+$totalTrackLineMass*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*$iTrack+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
						if {$spanTag== $numSpan} {
							set tempMass [expr $totalTrackLineMass*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*$iTrack+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*$iTrack+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $totalTrackLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*$iTrack+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
							set tempMass [expr $totalTrackLineMass*0.5*0.5*$deckEndTransitionLength+$totalTrackLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*$iTrack+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;
							set tempMass [expr $totalTrackLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*$iTrack+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0;		
					}
				}
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________ 
				# Define the nodes above the deck for the two line of top nodes of rails for track 1 on right.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
							set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+4)+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x + $railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+4)+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;	
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempMass [expr $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+4)+6]  [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempMass [expr $railLineMass*0.5*$deckEleLength + $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							} else {
									set tempMass [expr $railLineMass*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+4)+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+4)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodes on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+4)+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						if {$spanTag== $numSpan} {
							set tempMass [expr $railLineMass*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+4)+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+4)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+4)+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+4)+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;		
					}
				}		
					
				# _______________________________________________________________________________________________________
				# _______________________________________________________________________________________________________	
				# Define the nodes above the deck for the two line of top nodes of rails for track 2 on left.
				for {set iRail 1} {$iRail <= 2} {incr iRail} {
					# Start to define the nodes on the left part of the span. (with last digit 1 2 3 4)
					if {[expr $spanTag%$numSpanContinuous]!= 1 || $spanTag== 1} {
						# The case, when left end is not around an expansion joint.
							set pierTag [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*(0.5*$gapSEJ+$deckEndDiaphragmLength)];
						load [expr $spanTag*10000+1000*($iRail+6)+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*(0.5*$gapSEJ+$deckEndDiaphragmLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength];
					} else {
						# The case, when left end is indeed around an expansion joint.
						set pierTag  [expr $spanTag-1]; # Left pier of a certain span.
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+1] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x + $railLineMass*0.5*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+2] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+3] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEleLength];
						load [expr $spanTag*10000+1000*($iRail+6)+4] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						set x_temp [expr $x_B($pierTag)+$isolatorLoc_x+1.0*$deckEndTransitionLength];
					}	

						# Start to define the nodes of the mid-span deck. (with last two digits starting from 15)
						set i 1; 
						set ne_deckMiddle [expr max(round($deckMiddleLength/$deckEleLength),1)];
						while {$i<=$ne_deckMiddle} {
							# set x_temp [expr $x_temp + $deckEleLength];
							# For the last element which may have different length from other elements
							# set x_tempLast [expr $x_B($pierTag)+0.5*$gapSEJ+$deckEndDiaphragmLength+1.0*$deckEndTransitionLength+$deckMiddleLength]; 
							# For the last element which may have different length from other elements
							if {$i == $ne_deckMiddle} {
									set tempMass [expr $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)+$railLineMass*0.5*0.5*$deckEndTransitionLength];
								load [expr $spanTag*10000+1000*($iRail+6)+6]  [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							} elseif {$i == $ne_deckMiddle-1} {
									set tempMass [expr $railLineMass*0.5*$deckEleLength + $railLineMass*0.5*($deckMiddleLength-($ne_deckMiddle-1)*$deckEleLength)];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							} else {
									set tempMass [expr $railLineMass*1.0*$deckEleLength];
								load [expr $spanTag*10000+1000*($iRail+6)+10+$i] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							}	
							set i [expr $i+1];
							};
						# set tempNodeTag [expr $spanTag*10000+1000*($iRail+6)+10+$i];
						# set x_temp $x_tempLast;
						# unset x_tempLast;
						# unset x_temp;
						unset i;
						
						# Start to define the nodal load on the right part of the span.(with last digit 6 7 8 9)
					if {[expr $spanTag%$numSpanContinuous]!= 0  || $spanTag== $numSpan} {
						# The case, when right end is not around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$deckEndDiaphragmLength];
						load [expr $spanTag*10000+1000*($iRail+6)+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						if {$spanTag== $numSpan} {
							set tempMass [expr $railLineMass*1.0*$deckEndDiaphragmLength];
							load [expr $spanTag*10000+1000*($iRail+6)+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
						}; # For the right abutment of the last span			
					} else {
						# The case, when right end is around an expansion joint.
						set pierTag [expr $spanTag]; # Right pier of a certain span.
						#load [expr $spanTag*10000+1000*($iRail+6)+6]  $tempMass $tempMass $tempMass 0 0 0;
							set tempMass [expr $railLineMass*0.5*1.0*$deckEndTransitionLength];
						load [expr $spanTag*10000+1000*($iRail+6)+7] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*0.5*$deckEndTransitionLength+$railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+8] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;
							set tempMass [expr $railLineMass*0.5*$isolatorLoc_x];
						load [expr $spanTag*10000+1000*($iRail+6)+9] [expr $tempSa_x*$tempMass] [expr $tempSa_y*$tempMass] 0 0 0 0 ;	
					}
				}	

			}
		}
	};
};#End of if gamma_11 > 0;
#________________________________________________________________________________________
#########################################################################################
############################# END of Patterns for Structural Design######################
#########################################################################################
#________________________________________________________________________________________


#########################################################################################
############################# LLRM (Vertical and ??Longitudinal??)#######################
#########################################################################################
