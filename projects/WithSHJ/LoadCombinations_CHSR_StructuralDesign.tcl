# This file is to define the load combinations for design of Structures in CHSR Project
# Developed by Yong Li, University of California, San Diego
# Developed Date: May 15th, 2012
# Contact: foxchameleon@gmail.com, yongli@ucsd.edu
# Modified: 
#         1. 

# Load Combinations:
# LRDR Load Cases:
# Strength 1, Strength 2, Strength 3, Strength 4, Strength 5, Extreme 1, Extreme 2, Extreme 3, Service 1, Service 2,Service 3,...
# Track Serviceability Load Cases:
# Group 1a, Group 1b, Group 1c, Group 2, Group 3
# Rail-Structure Interaction Analysis:
# Group 4, Group 5.


#########################################################################################
####################################### LC_GravityAlone  ################################
#########################################################################################
if {$LCTYPE == "LC_GravityAlone" } {
	set gamma_1 1.0;
	set gamma_2 0;
	set gamma_4 0.0;
	set gamma_5 0.0;
	set gamma_11 0.0;
};


#########################################################################################
######################################### Strength 1 ####################################
#########################################################################################
if {$LCTYPE == "LC_Strength1_1" } {
	set gamma_1 1.0;

	set gamma_2 1.75;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;

	
};
#########################################################################################
######################################### Strength 2 ####################################
#########################################################################################
if {$LCTYPE == "LC_Strength2_1" } {
	set gamma_1 1.25;

	set gamma_2 0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 1.4;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Strength 3 ####################################
#########################################################################################
if {$LCTYPE == "LC_Strength3_1" } {
	set gamma_1 1.5;

	set gamma_2 0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Strength 4 ####################################
#########################################################################################
if {$LCTYPE == "LC_Strength4_1" } {
	set gamma_1 1.25;

	set gamma_2 1.35;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.65;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 1.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Strength 5#####################################
#########################################################################################
if {$LCTYPE == "LC_Strength5_1" } {
	set gamma_1 1.25;

	set gamma_2 0.5;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 1.1;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};

if {$LCTYPE == "LC_Strength5_2" } {
	set gamma_1 1.25;

	set gamma_2 0.5;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 1.1;
	set EQHazardLevel "OBE";
	set yxCombinationCase "X";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Extreme 1 #####################################
#########################################################################################
if {$LCTYPE == "LC_Extreme1_1" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};

#########################################################################################
######################################### Extreme 2 ####################################
#########################################################################################
if {$LCTYPE == "LC_Extreme2_1" } {
	set gamma_1 1.0;

	set gamma_2 0.5;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Extreme 3 #####################################
#########################################################################################
if {$LCTYPE == "LC_Extreme3_1" } {
	set gamma_1 1.0;

	set gamma_2 0.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.

	set gamma_11 1.0;
	set EQHazardLevel "MCE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};

if {$LCTYPE == "LC_Extreme3_2" } {
	set gamma_1 1.0;

	set gamma_2 0.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.

	set gamma_11 1.0;
	set EQHazardLevel "MCE";
	set yxCombinationCase "X";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Service 1  ####################################
#########################################################################################
if {$LCTYPE == "LC_Service1_1" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.45;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 1.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Service 2 #####################################
#########################################################################################
if {$LCTYPE == "LC_Service2_1" } {
	set gamma_1 1.0;

	set gamma_2 1.30;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Service 3 #####################################
#########################################################################################
if {$LCTYPE == "LC_Service3_1" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Group 1a  #####################################
#########################################################################################
if {$LCTYPE == "LC_Group_1a" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 0.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    0.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 0.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Group 1b ######################################
#########################################################################################
if {$LCTYPE == "LC_Group_1b" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 0.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 0.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Group 1c ######################################
#########################################################################################
if {$LCTYPE == "LC_Group_1c" } {

	
};
#########################################################################################
######################################### Group 2  ######################################
#########################################################################################
if {$LCTYPE == "LC_Group_2_1" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 0.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    0.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 1.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 1.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
};
#########################################################################################
######################################### Group 3  ######################################
#########################################################################################
if {$LCTYPE == "LC_Group_3_1" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 0.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    0.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 1.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};


if {$LCTYPE == "LC_Group_3_2" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 0.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    0.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 1.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "X";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
};
#########################################################################################
######################################### Group 4  ######################################
#########################################################################################
if {$LCTYPE == "LC_Group_4_1" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    1.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 0.0;
	set EQHazardLevel "MCE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};
#########################################################################################
######################################### Group 5  ######################################
#########################################################################################
if {$LCTYPE == "LC_Group_5_1" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    0.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 1.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "Y";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;
	
};

if {$LCTYPE == "LC_Group_5_2" } {
	set gamma_1 1.0;

	set gamma_2 1.0;
	# Track #1
	set LLVyesOrno_1    1.0;
	set LLRRyesOrno_1   0.0;
	set startingLoc_1   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_1     [expr $startingLoc_1 + $trainLength];
	set leftLoc_1       [expr max($startingLoc_1,0)];
	#set rightLoc_1      [expr min($endingLoc_1,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_1      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_1 [expr $rightLoc_1-$leftLoc_1];
	set direction_1     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_1 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction
	# Track #2
	set LLVyesOrno_2    0.0;
	set LLRRyesOrno_2   0.0;
	set startingLoc_2   0; # The coordinates to locate the trainset on the bridge.
	set endingLoc_2     [expr $startingLoc_2 + $trainLength];
	set leftLoc_2       [expr max($startingLoc_2,0)];
	#set rightLoc_2      [expr min($endingLoc_2,[expr $numSpan*$typicalSpanLength])];
	set rightLoc_2      [expr [expr $numSpan*$typicalSpanLength]];
	set loadingLength_2 [expr $rightLoc_2-$leftLoc_2];
	set direction_2     [expr 1.0]; # 1.0-->x; -1.0 <--X
	set tractionBrake_2 [expr 1.0]; # 1.0 Braking, -1.0 Traction; 0 No Braking and Traction

	set gamma_4 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	set gamma_5 0.0;
	set windDirection       [expr 1.0]; # 1.0 in y direction, -1.0 in -y direction.
	
	set gamma_11 1.0;
	set EQHazardLevel "OBE";
	set yxCombinationCase "X";
	set EQDirection_x 1; # 1 0r -1;
	set EQDirection_y 1; # 1 0r -1;	
};
