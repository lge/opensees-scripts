proc DefineLoadPattern_UniformEQ {iLoadTag iGMdirection iGMfilename args} {
	########################################################
	#  DefineLoadPattern_UniformEQ $iLoadTag $iGMdirection $iGMfilename
	########################################################
	# define load pattern for ground-motion analysis -- uniform-support EQ excitation, unidirectional & bidirectional
	# define nodal accelerations for uniform-support excitation
	#	   Silvia Mazzoni, 2008 (mazzoni@berkeley_NO_SPAM_.edu)
	##   LoadTag:	unique load tag
	##   GMdirection 		# lateral dof for ground motion input
	##   GMfilename 		# ground-motion filename for input-- support PEER strong motion database files with dt in header
	#
	global g cm
	set iDefaultValue "GMdirectory . GMfactor 1.0  FileType Time&Acceleration NumberHeaderLines 0 LoadFactor 1.0"
	foreach {Name DefaultValue} $iDefaultValue {
		set $Name $DefaultValue
	}
	# override defaults with anything specified in argument list:
	foreach {Option Value} $args {
		if {[string index $Option 0]=="-"} {;							# remove the hyphen
			set Option [string range $Option 1 end]
		}
		set $Option $Value
		if {[llength $Value]>1} {;											# if the argument is a list
			foreach {SubName SubValue} $Value {
				if {[string index $SubName 0]=="-"} {;				# remove the hyphen
					set SubName [string range $SubName 1 end]
				}
				set $SubName $SubValue
			}
		}
	}
	# --------------------------------- make lists the same size
	if {[info exists iGMfactor]!=1} {
		set iGMfactor ""
		foreach SupportNode $iGMdirection {
			lappend iGMfactor $GMfactor
		}
	}
	if {[llength $iLoadTag]!=[llength $iGMdirection]} {
		puts stderr "ERROR: iLoadTag and  and iGMdirection must have the same number of elements"
		return -1
	}
	set iListName "iGMdirection iGMfilename iGMfactor"
	foreach ListName $iListName {
		eval "set theList $$ListName"
		if {[llength $theList]==1} {
			set OneValue $theList
			set theList ""
			foreach SupportNode $iGMdirection {
				lappend theList $OneValue
			}
			set $ListName $theList
		} elseif {[llength $theList]!=[llength $iGMdirection]} {
			puts stderr "ERROR: $ListName and iGMdirection must have the same number of elements"
			return -1
		}
	}
	# ---------------------------------
	# ------------------------------------------------
	# the following commands are unique to the Uniform Earthquake excitation: acceleration input
	foreach LoadTag $iLoadTag GMdirection $iGMdirection GMfilename $iGMfilename GMfactor $iGMfactor {
		set inFile $GMdirectory/$GMfilename
		if {$FileType == "Time&Acceleration" } {					# time and input are in two columns of one file
			set AccelSeries "Series -file $inFile -factor [expr $LoadFactor*$GMfactor]";		# time series information
		} elseif {$FileType == "AccelerationOnly" } {	;			# only acceleration is specified
			if {[info exists dt]!=1} {
				puts stderr "ERROR: Need to specify dt when using a FileType AccelerationOnly "
				return -1
			};	# this variable is used below
			set GMfatt [expr $g*[expr $LoadFactor*$GMfactor]];			# data in input file is in g Unifts -- ACCELERATION TH
			set AccelSeries "Series -dt $dt -filePath $inFile -factor  $GMfatt";		# time series information
		} elseif {$FileType == "PEER" } {		;		# best to specify g -- four rows of header lines where dt is specified in line 4
			set outFile $GMdirectory/Mod${GMfilename};			# set variable holding new filename
			ReadSMDFile $inFile $outFile dt;			# call procedure to convert the ground-motion file
			if {[info exists g]!=1} {
				puts stderr "ERROR: need to define g to scale PEER ground motions"
				return -1
			};	# this variable is used below
			set GMfatt [expr $g*[expr $LoadFactor*$GMfactor]];			# data in input file is in g Unifts -- ACCELERATION TH
			set AccelSeries "Series -dt $dt -filePath $outFile -factor  $GMfatt";		# time series information
		} elseif {$FileType == "PEERNGA" } {		;		# best to specify g -- four rows of header lines where dt is specified in line 4
			set outFile $GMdirectory/Mod${GMfilename};			# set variable holding new filename
			ReadPEERNGAFile $inFile $outFile dt;			# call procedure to convert the ground-motion file
			if {[info exists g]!=1} {
				puts stderr "ERROR: need to define g to scale PEER-NGA ground motions"
				return -1
			};	# this variable is used below
			set GMfatt [expr $g*[expr $LoadFactor*$GMfactor]];			# data in input file is in g Unifts -- ACCELERATION TH
			set AccelSeries "Series -dt $dt -filePath $outFile -factor  $GMfatt";		# time series information
		} else {
			puts stderr "ERROR: FileType $FileType is not recognized. Options: Time&Acceleration, AccelerationOnly, PEER, PEERNGA"
			return -1
		}
		pattern UniformExcitation  $LoadTag  $GMdirection -accel  $AccelSeries  ;	# create Unifform excitation
	};	# end foreach LoadTag.......
}
