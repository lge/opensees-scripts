# Note: Apply Damping to the corresponding structure components.
# Need Files: 
	# 1. Analyze.Eigen.tcl
	
# Developed by: Yong LI, University of California, San Diego, foxchameleon@gmail.com
# Date: 2012.01.06 while working on California High-Speed Rail Bridge Project
# Modified Diary:
	# 2012.01.06:
	
# define DAMPING and apply Rayleigh DAMPING
# D = $alphaM*M + $betaKcurr*Kcurrent + $betaKcomm*KlastCommit + $beatKinit*$Kinitial
set ksi 0.02; # damping ratio
set MpropSwitch 1.0;
set KcurrSwitch 0.0;
set KcommSwitch 0.0;
set KinitSwitch 1.0;
set nEigenI 1;		# mode I
set nEigenJ 1;		# mode J
set nEigenModes 1;

# Only valid for higher version like 2.2.0 rather than 1.7.0
# set lambdaN [eigen -fullGenLapack $nEigenModes];			# eigenvalue analysis for nEigenModes modes

source Analyze.Eigen.tcl; # return lambda
set lambdaN $lambda;

set lambdaI [lindex $lambdaN [expr $nEigenI-1]];    # eigenvalue mode i
set lambdaJ [lindex $lambdaN [expr $nEigenJ-1]]; 	# eigenvalue mode j
set omegaI [expr pow($lambdaI,0.5)];
set omegaJ [expr pow($lambdaJ,0.5)];
set TI [expr (2.*$PI)/$omegaI];
set TJ [expr (2.*$PI)/$omegaJ];
puts "Apply Damping to this two modes with Periods: T = $TI $TJ"

set alphaM [expr $MpropSwitch*$ksi*(2*$omegaI*$omegaJ)/($omegaI+$omegaJ)];	# M-prop. damping; D = alphaM*M
set betaKcurr [expr $KcurrSwitch*2.*$ksi/($omegaI+$omegaJ)];         		# current-K;      +beatKcurr*KCurrent
set betaKcomm [expr $KcommSwitch*2.*$ksi/($omegaI+$omegaJ)];   		        # last-committed K;   +betaKcomm*KlastCommitt
set betaKinit [expr $KinitSwitch*2.*$ksi/($omegaI+$omegaJ)];         		# initial-K;     +beatKinit*Kini
rayleigh $alphaM $betaKcurr $betaKinit $betaKcomm; 