# Note: Do Eigen Analysis after the model is built, specifically after the gravity analysis!
# Developed by: Yong LI, University of California, San Diego, foxchameleon@gmail.com
# Date: 2012.01.06 while working on California High-Speed Rail Bridge Project
# Modified Diary: Modified from 13_EigenAnalysis.tcl file for Margar_Marga Bridge Project in Chile.
	# 2012.01.06:
puts "Yong: staring Eigen Analysis in Analyze.Eigen.tcl"
	
# Eigen Analysis.
file mkdir modes; #Directory to store data for eigen analysis.
set numModes 2;

# Define Recorders for the Eigen Modes.
# for { set k 1 } { $k <= $numModes } { incr k } {
    # recorder Node -file [format "modes/mode%i.out" $k] -nodeRange 401 407 -dof 1 2 3  "eigen $k";
# } 
# recorder Node -file mode1.out -nodeRange 401 407 -dof 1 2 3  "eigen 1";

# Command Example: eigen <$type> <$solver> $numEigenvalues
# $type: optional string detailing type of eigen analysis: -standard or -generalized (default: -generalized) 
# $solver: -genBandArpack, -symmSparseArpack, -symmBandLapack, -fullGenLapack, -UmfPack, -SuperLU 
set lambda [eigen -generalized -SuperLU $numModes]; 

# #############  NOTE for the MargaMarga Bridge: Possible Bug for Eigen and Penalty ###################
# -UmfPack and -genBandArpack work fine for the first and second eigen analysis when "Penalty " and " analysis" are not claimed. 
# -SuperLU works fine for the first eigen analysis but not the second eigen analysis when "pattern" is claimed. But If we just don't do eigen before "pattern" is claimed, the eigen works fine. 
# The other options do not work!
# ################################################################## 

set omega {};
set F {};
set T {};
set PI [expr 2.0*asin(1.0)];

foreach lam $lambda {
	lappend omega [expr sqrt($lam)]; # set omega [expr pow($lam,0.5)];
	lappend F [expr sqrt($lam)/(2*$PI)];
	lappend T [expr (2*$PI)/sqrt($lam)];
}

# Write out the Periods.
set period "modes/Periods.out"
set fileID [open $period "w"];
set temp_i 0;
foreach t $T {
	incr temp_i;
	puts "period of mode: $temp_i $t sec"; # Print Period to screen.
    puts $fileID " $t";
}
close $fileID;

# Write out the Frequencies.
set Frequencies "modes/Frequencies.out"
set fileID [open $Frequencies "w"];
set temp_i 0;
foreach f $F {
	incr temp_i;
	puts "Frequency of mode: $temp_i $f Hz"; # Print Period to screen.
    puts $fileID " $f";
}
close $fileID;