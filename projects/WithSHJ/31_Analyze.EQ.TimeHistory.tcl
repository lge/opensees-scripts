# Note: to do time hisotry analyis.
# Needs: 
	# 1. ApplyDamping.tcl
	# 2. 13_RecorderFileFromMatlab.tcl
	# 3. 32_Analyze.EQ.TimeHisotory.ConvergenceOpt.tcl
	
# Modified By Yong Li, August 2010, Univerisity of California at San Diego. 
# Contact: foxchameleon@gmail.com / yongli@ucsd.edu
# Modified for CHSR Isolator Project, on 2012.01.06
# Modified for CHSR Isolator Project, on 2012.01.18 for Solution Summary.

####################################################################################################
# Uniform Earthquake ground motion (uniform acceleration input at all support nodes)
# --------------------------------------------------------------------------------------------------
# execute this file after you have built the model, and after you apply gravity
# --------------------------------------------------------------------------------------------------

loadConst -time 0.0;    	          # keep previous loading constant and reset pseudo-time to zero

# DYNAMIC ANALYSIS PARAMETERS
# source 30_LibAnalysisDynamicParameters.tcl; # constraintsHandler,DOFnumberer,system-ofequations,convergenceTest,solutionAlgorithm,integrator
# Below is a simple version for the above source file
# wipeAnalysis
# constraints Transformation ; 
# numberer Plain;
# system BandGeneral;
# test NormDispIncr 1.0e-6 10 2; 
# algorithm Newton;        
# integrator Newmark 0.5 0.25; 
# analysis Transient;
# ______________________________  Apply Rayleigh Damping to the model _________________________________________
source ApplyDamping.tcl; 

##################################  Perform Dynamic Ground-Motion Analysis ######################################
# ______________________________  Define the Uniform Excitation Pattern _________________________________________
set IDLoadTag 400;      #Set a number different from other loads at your will for uniform Support excitation
# Uniform EXCITATION: acceleration input: Option1 or Option 2
#Option1(Complicated): for multiply earthquakes analysis
set GMdir ".";
source ReadPEERNGAFile.tcl;
# source ReadSMDFile.tcl;
# source ReadRecordNGA_SMD.tcl;
foreach GMdirection $iGMdirection GMfile $iGMfile GMfact $iGMfact {
	incr IDLoadTag;
	# set inFile $GMdir/$GMfile.at2;
	set inFile $GMdir/$GMfile.AT2;
	set outFile $GMdir/$GMfile.g3;
	set inputFile $outFile;
	puts $GMdir;
	
	ReadPEERNGAFile $inFile $outFile DT NPTS;		# call procedure to convert the ground-motion file
	#Note: Here dt and npts will be set values in the command ReadPEERNGAFile
	set npts $NPTS;
	set dt $DT;
	
	set unitGM $g;
	set GMfact [expr $unitGM*$GMfact];	# data in input file is in g Units -- ACCELERATION TH
	puts "GMdirection $GMdirection GMfile $GMfile GMfact $GMfact NPTS $npts DT $dt";
	puts "GMfact  = $GMfact ";
	
	#set AccelSeries "Series -dt $dt -filePath $inputFile -factor  $GMfact";		# time series information
	#pattern UniformExcitation  $IDLoadTag  $GMdirection -accel  $AccelSeries  ;	# create Unifform excitation
	
	# To record the absolute acceleration directly after defining timeSeries and if in the recorder we put it this way: 
	# "recorder Node -file nodesA.out -timeSeries 1 -time -node 1 2 3 4 -dof 1 accel;" 	
	timeSeries Path $IDLoadTag -dt $dt -filePath $inputFile -factor $GMfact;
	#Commnad Example: pattern UniformExcitation $patternTag $dir -accel $tsTag <-vel0 $ver0>
	pattern UniformExcitation $IDLoadTag $GMdirection -accel $IDLoadTag; # create Uniform excitation
}

# Option2 (Simple): for only one earthquake analysis
# set GMdirection 1;				      # ground-motion direction
# set GMfile "elcentro1940.txt" ;	  # ground-motion filenames
# set npts 4001;
# set GMfact [expr $g*1.5];			  # data in input file is in g Unifts -- ACCELERATION TH
# set dt 0.01;			              # time step for input ground motion
# set AccelSeries "Series -dt $dt -filePath $GMfile -factor  $GMfact";	            # time series information
# pattern UniformExcitation  $IDLoadTag  $GMdirection -accel  $AccelSeries  ;		# create Unifform excitation

# ______________________________  Define the Recorders _________________________________________
# source 13_RecorderFile.tcl; # Need to be placed after the timeSeries Definition to record absolute acceleration.

# ___________________  Apply Basic Dyanmic Analysis using basic analysis options set up _____________________
# set up ground-motion-analysis parameters
set DtAnalysis	      [expr $dt];	            # time-step Dt for lateral analysis WHICH SHOULD BE LESS THAN dt
set TmaxAnalysis      [expr $npts*$dt*$sec];	# duration of ground-motion analysis
set Nsteps            [expr int($TmaxAnalysis/$DtAnalysis)];



algorithm $algorithmTypeDynamic;
puts "algorithm $algorithmTypeDynamic"
set ok [analyze $Nsteps $DtAnalysis];			# actually perform analysis; returns ok=0 if analysis was successful

#Output Nodal Solution Information
set dataDir .;
set SolutionDataFile [open "$dataDir/SolutionDataFile.out" "w"];
# Output default solution set up results.
set controlTime [getTime];
puts $SolutionDataFile "Default Solution Results, at $controlTime ok=$ok";
puts $SolutionDataFile "constraints=$constraintsTypeDynamic";
puts $SolutionDataFile "numberer=$numbererTypeDynamic";
puts $SolutionDataFile "system=$systemTypeDynamic";
puts $SolutionDataFile "test=$testTypeDynamic $TolDynamic $maxNumIterDynamic";
puts $SolutionDataFile "algorithm=$algorithmTypeDynamic";
puts $SolutionDataFile "integrator=$integratorTypeDynamic";
puts $SolutionDataFile "analysisTimeStep=$DtAnalysis";
close $SolutionDataFile;

if {$ok == 0} {
	puts "Ground Motion Done successfully. End Time: [getTime]"
} elseif {$ok != 0} {
	puts "Ground Motion stopped unsuccessfully. End Time: [getTime]"
	# If confronted with convergence problems, try following shceme.
	source 32_Analyze.EQ.TimeHisotory.ConvergenceOpt.tcl;
}
	







