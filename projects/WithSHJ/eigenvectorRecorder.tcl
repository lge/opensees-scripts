#Output Mode Shape Information
set dataDir .;
set nummodes [llength $lambda]
for { set imode 1} { $imode <= $nummodes } { incr imode 1} {
	set EigenVectorDataFile [open "$dataDir/EigenVectorDataFile_$imode.out" "w"];
	set NodeTags [getNodeTags];
	set numNodes [llength $NodeTags];
	for { set inode 1} { $inode <= $numNodes } { incr inode 1} {
		set nodei [lindex $NodeTags [expr $inode-1]]
		set X [nodeEigenvector $nodei $imode 1]
		set Y [nodeEigenvector $nodei $imode 2]
		set Z [nodeEigenvector $nodei $imode 3]
		set theX [nodeEigenvector $nodei $imode 4]
		set theY [nodeEigenvector $nodei $imode 5]
		set theZ [nodeEigenvector $nodei $imode 6]
		#puts "node $nodei $aX $aY $aZ  "
		puts -nonewline $EigenVectorDataFile "$nodei $X $Y $Z $theX $theY $theZ \n";
	}
	close $EigenVectorDataFile;
}