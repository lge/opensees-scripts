wipe;
model basic -ndm 2 -ndf 3
geomTransf Linear 1;
geomTransf PDelta 2;
geomTransf Corotational 3;
node 1 760 640 -mass 3976 3976 3976;
node 2 760 360 -mass 3976 3976 3976;

#uniaxialMaterial Elastic 1 29000;

section Elastic 1 29000 100 833.3333;

element dispBeamColumn 1 1 2 6 1 3;
#element nonlinearBeamColumn 1 1 2 6 1 3;
fix 2 1 1 1;
timeSeries Linear 1 -factor 1;
pattern Plain 1 1 {;
load 1 100 0 0;
};
constraints Plain;
numberer Plain;
system BandGeneral;
test NormDispIncr 1e-8 15;
algorithm Newton;
integrator LoadControl 0.1;
analysis Static;
analyze 1
puts [nodeDisp 1]
