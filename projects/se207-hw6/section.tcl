# Core concrete:
uniaxialMaterial Concrete01 1        $fc   $str    $fccru   $strcru  
  
# Unfined concrete:
uniaxialMaterial Concrete01 2        $unfc  $unstr $unfccru  $unstrcru    

# Steel:
uniaxialMaterial Steel02 3 $fy $E $b $R0 $R1 $R2 $a1 $a2 $a3 $a4

# Elastic Core concrete:
uniaxialMaterial Elastic 4 [expr 2*$fc/$str]
  
# Elastic Unfined concrete:
uniaxialMaterial Elastic 5 [expr 2*$unfc/$unstr]

# Elastic Steel:
uniaxialMaterial Elastic 6 $E


# Column
section Fiber 1 {
    patch rect $unconfine_concrete_matTag       [expr 2*$refine_factor]         1            $y2  $z2  [expr $y2-$cover] [expr -$z2]
    patch rect $core_concrete_matTag       [expr 12*$refine_factor]        1           [expr $y2-$cover] $z2 [expr -$y2+$cover] [expr -$z2]
    patch rect $unconfine_concrete_matTag       [expr 2*$refine_factor]          1           [expr -$y2+$cover] $z2 [expr -$y2] [expr -$z2]

    layer straight $steel_matTag       4         $As         [expr $y2-$cover] [expr $z2-$cover] [expr $y2-$cover] [expr -$z2+$cover]
    layer straight $steel_matTag       2         $As         0.0 [expr $z2-$cover] 0.0 [expr -$z2+$cover]
    layer straight $steel_matTag       4         $As         [expr -$y2+$cover] [expr $z2-$cover] [expr -$y2+$cover] [expr -$z2+$cover]
}

# Beam
section Fiber 2 {
    patch rect $unconfine_concrete_matTag       [expr 2*$refine_factor]          1            $y1  $z1  [expr $y1-$cover] [expr -$z1]
    patch rect $core_concrete_matTag       [expr 8*$refine_factor]          1           [expr $y1-$cover] $z1 [expr -$y1+$cover] [expr -$z1]
    patch rect $unconfine_concrete_matTag       [expr 2*$refine_factor]          1           [expr -$y1+$cover] $z1 [expr -$y1] [expr -$z1]

    layer straight $steel_matTag       4         $As         [expr $y1-$cover] [expr $z1-$cover] [expr $y1-$cover] [expr -$z1+$cover]
    layer straight $steel_matTag       2         $As         0.0 [expr $z1-$cover] 0.0 [expr -$z1+$cover]
    layer straight $steel_matTag       4         $As         [expr -$y1+$cover] [expr $z1-$cover] [expr -$y1+$cover] [expr -$z1+$cover]
}

				


 


