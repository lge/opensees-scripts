set PCol [expr -$pratio*$Ag*$unfc]
set ground_left_node 1
set first_left_node [expr $nele_per_col+1]
set second_left_node  [expr 2*$nele_per_col+1]
set second_right_node [expr 2*$nele_per_col+1+$nele_per_beam]
set first_right_node  [expr 3*$nele_per_col+1+$nele_per_beam]
set ground_right_node [expr 4*$nele_per_col+1+$nele_per_beam]
set endNodeNum [expr $ground_right_node+$nele_per_beam-1]
set deltaY [expr $Lc/$nele_per_col]
set deltaX [expr $Lb/$nele_per_beam]

if {$nele_per_beam > 1} {
	set first_beam_internal_left_node [expr $ground_right_node+1]
} else {
	set first_beam_internal_left_node -1
}

set i 1
set X 0.0
set Y 0.0
while {$i <= $second_left_node} {
	node $i $X $Y
	set Y [expr $Y+$deltaY]
	incr i
}

set X $deltaX
set Y [expr $Lc*2]
while {$i <= $second_right_node} {
	node $i $X $Y
	set X [expr $X+$deltaX]
	incr i
}

set X $Lb
set Y [expr $Lc*2-$deltaY]
while {$i <= $ground_right_node} {
	node $i $X $Y
	set Y [expr $Y-$deltaY]
	incr i
}

if {$nele_per_beam > 1} {
	set X $deltaX
	set Y $Lc
	while {$i <= $endNodeNum} {
		node $i $X $Y
		set X [expr $X+$deltaX]
		incr i
	}
}

fix $ground_left_node 1 1 1;
fix $ground_right_node 1 1 1;

set i 1
while {$i < $second_left_node} {
	element $col_etype $i $i [expr $i+1] $col_nintpts $ColSecTag $ColTransfTag;
	incr i
}

while {$i < $second_right_node} {
	element $beam_etype $i $i [expr $i+1] $beam_nintpts $BeamSecTag $BeamTransfTag;
	incr i
}

while {$i < $ground_right_node} {
	element $col_etype $i $i [expr $i+1] $col_nintpts $ColSecTag $ColTransfTag;
	incr i
}

if {$first_beam_internal_left_node < 0} {
	element $beam_etype $i $first_left_node $first_right_node $beam_nintpts $BeamSecTag $BeamTransfTag
	set eleHasSectionB $i
} else {
	element $beam_etype $i $first_left_node $first_beam_internal_left_node $beam_nintpts $BeamSecTag $BeamTransfTag;
	set eleHasSectionB $i
	incr i;
	set j $first_beam_internal_left_node
	while {$j < $endNodeNum} {
		element $beam_etype $i $j [expr $j+1] $beam_nintpts $BeamSecTag $BeamTransfTag;
		incr j
		incr i
	}
	element $beam_etype $i $endNodeNum $first_right_node $beam_nintpts $BeamSecTag $BeamTransfTag;
}

# file mkdir $dataDir
#puts "datadir:$dataDir"
recorder Node -tcp 127.0.0.1 8124 -time -node $second_right_node -dof 1 disp;
# recorder Node -file $dataDir/floor2_dispx.out -time -node $second_right_node -dof 1 disp;
# recorder Node -file $dataDir/floor2_accx.out -time -node $second_right_node -dof 1 accel;
# recorder Node -file $dataDir/base_shear.out -time -node $ground_left_node $ground_right_node -dof 1 reaction;
# recorder Element -file $dataDir/sectionA_deform.out -ele 1 section 1 deformation
# recorder Element -file $dataDir/sectionA_force.out -ele 1 section 1 force
# recorder Element -file $dataDir/sectionB_deform.out -ele $eleHasSectionB section 1 deformation
# recorder Element -file $dataDir/sectionB_force.out -ele $eleHasSectionB section 1 force
# recorder Element -file $dataDir/floor1_left_col_endforce.out -eleRange 1 $nele_per_col localForce
# recorder Element -file $dataDir/floor2_left_col_endforce.out -eleRange [expr 1+$nele_per_col] [expr 2*$nele_per_col] localForce
# recorder Element -file $dataDir/floor1_left_col_sectionforce.out -eleRange 1 $nele_per_col section force
# recorder Element -file $dataDir/floor2_left_col_sectionforce.out -eleRange [expr 1+$nele_per_col] [expr 2*$nele_per_col] section force














