close all
dirs={'force_1_5_p005_nonlinear' 'force_1_5_p005_linelastic'};
ele_conf='force-based element';
lstr={'nonlinear','linear elastic'};
Lcol=4;
f1=figure();
f2=figure();
f3=figure();
P=0.05*0.64*30e6;
markers={'-r' '--k' '-.b'};
for i=[1 2]
	
    disp=load([dirs{i} '/floor2_dispx.out']);
	acc=load([dirs{i} '/floor2_accx.out']);
	base_shear=load([dirs{i} '/base_shear.out']);
    
    figure(f1);
    hold on;
    plot(disp(:,1),disp(:,2),markers{i});
    
    
    figure(f2);
    hold on;
    plot(acc(:,1),acc(:,2),markers{i});
	
	figure(f3);
    hold on;
    plot(base_shear(:,1),(base_shear(:,2)+base_shear(:,3))/3/P,markers{i});
   
end

figure(f1);
title(sprintf(['Roof node displacement response\n',ele_conf]));
xlabel('Time (s)');
ylabel('Horizontal displacement (m)');
grid on;
legend(lstr,'Location','SouthEast');

figure(f2);
title(sprintf(['Roof node absolute acceleration response\n',ele_conf]));
xlabel('Time (s)');
ylabel('Absolution acceleration (m/s^2)');
grid on;
legend(lstr,'Location','SouthEast');

figure(f3);
title(sprintf(['Total base shear response\n',ele_conf]));
xlabel('Time (s)');
ylabel('Total base shear (normalized by total gravtity load 3P)');
grid on;
legend(lstr,'Location','SouthEast');

