set Hload 1.0
set iPushNode [list $first_left_node $first_right_node];
pattern Plain 200 Linear {
	foreach PushNode $iPushNode {
		load $PushNode $Hload 0.0 0.0
	}
}
set iPushNode [list $second_left_node $second_right_node];
pattern Plain 300 Linear {
	foreach PushNode $iPushNode {
		load $PushNode [expr $Hload*2] 0.0 0.0
	}
}

integrator DisplacementControl   $second_left_node   1  0.001  
test NormDispIncr 1.0e-8 1000 1
system BandGeneral
numberer Plain
constraints Plain
#algorithm Newton
algorithm NewtonLineSearch 0.5
#algorithm BFGS
#algorithm Broyden 100
analysis Static

set ok 0
set step 1

while {$ok ==0 & $step<=320} {
	set ok [analyze 1]
    # if {$ok != 0} {
    #     test NormDispIncr 1.0e-6 1000 1
    #     set ok [analyze 1]
    # } else {
    #     test NormDispIncr 1.0e-8 1000 1
    # }  
	set step [expr $step+1]
	puts "step $step"
}
if {$ok == 0} {
    open "$dataDir/ok.txt"  "w"
    
} else {
    open "$dataDir/fail_at_$step.txt"  "w"
}



				


 


