
puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Static Pushover Analysis --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Static.Push.tcl
