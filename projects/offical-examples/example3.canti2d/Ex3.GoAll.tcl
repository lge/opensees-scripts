
puts " -------Elastic Model -------"
puts " -------Static Pushover Analysis -------"
source Ex3.Canti2D.build.ElasticElement.tcl
source Ex3.Canti2D.analyze.Static.Push.tcl

puts " -------Elastic Model -------"
puts " -------Uniform Earthquake Excitation -------"
source Ex3.Canti2D.build.ElasticElement.tcl
source Ex3.Canti2D.analyze.Dynamic.EQ.Uniform.tcl

puts " ------------------------------------------ -------"

puts " -------Uniaxial Inelastic Section, Nonlinear Model -------"
puts " -------Static Pushover Analysis -------"
source Ex3.Canti2D.build.InelasticSection.tcl
source Ex3.Canti2D.analyze.Static.Push.tcl

puts " -------Uniaxial Inelastic Section, Nonlinear Model -------"
puts " -------Uniform Earthquake Excitation -------"
source Ex3.Canti2D.build.InelasticSection.tcl
source Ex3.Canti2D.analyze.Dynamic.EQ.Uniform.tcl

puts " ------------------------------ -------"

puts " -------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model -------"
puts " -------Static Pushover Analysis -------"
source Ex3.Canti2D.build.InelasticFiberSection.tcl
source Ex3.Canti2D.analyze.Static.Push.tcl

puts " -------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model -------"
puts " -------Uniform Earthquake Excitation -------"
source Ex3.Canti2D.build.InelasticFiberSection.tcl
source Ex3.Canti2D.analyze.Dynamic.EQ.Uniform.tcl

