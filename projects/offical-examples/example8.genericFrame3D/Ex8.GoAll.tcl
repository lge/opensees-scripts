puts " ----------------- Steel W section ---------------"
puts " --  W-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Static Pushover Analysis --"
source Ex8.genericFrame3D.build.Wsec.tcl
source Ex8.genericFrame3D.analyze.Static.Push.tcl

puts " ----------------- Steel W section ---------------"
puts " --  W-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Static Reversed-Cyclic Analysis --"
source Ex8.genericFrame3D.build.Wsec.tcl
source Ex8.genericFrame3D.analyze.Static.Cycle.tcl

puts " ----------------- Steel W section ---------------"
puts " --  W-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Uniform Sine-wave Excitation --"
source Ex8.genericFrame3D.build.Wsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.sine.Uniform.tcl

puts " ----------------- Steel W section ---------------"
puts " --  W-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Uniform Earthquake Excitation --"
source Ex8.genericFrame3D.build.Wsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.EQ.Uniform.tcl

puts " ----------------- Steel W section ---------------"
puts " --  W-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Uniform Bidirectional Earthquake Excitation --"
source Ex8.genericFrame3D.build.Wsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.EQ.bidirect.tcl

puts " ----------------- Steel W section ---------------"
puts " --  W-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex8.genericFrame3D.build.Wsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.sine.multipleSupport.tcl

puts " ----------------- Steel W section ---------------"
puts " --  W-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Multiple-support Earthquake Excitation --"
source Ex8.genericFrame3D.build.Wsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.eq.multipleSupport.tcl

puts " --  ----------------------------------------------------------------- "

puts " ----------------- Reinforced Concrete section ---------------"
puts " --  RC-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Static Pushover Analysis --"
source Ex8.genericFrame3D.build.RCsec.tcl
source Ex8.genericFrame3D.analyze.Static.Push.tcl

puts " ----------------- Reinforced Concrete section ---------------"
puts " --  RC-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Static Reversed-Cyclic Analysis --"
source Ex8.genericFrame3D.build.RCsec.tcl
source Ex8.genericFrame3D.analyze.Static.Cycle.tcl

puts " ----------------- Reinforced Concrete section ---------------"
puts " --  RC-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Uniform Sine-wave Excitation --"
source Ex8.genericFrame3D.build.RCsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.sine.Uniform.tcl

puts " ----------------- Reinforced Concrete section ---------------"
puts " --  RC-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Uniform Earthquake Excitation --"
source Ex8.genericFrame3D.build.RCsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.EQ.Uniform.tcl

puts " ----------------- Reinforced Concrete section ---------------"
puts " --  RC-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Uniform Bidirectional Earthquake Excitation --"
source Ex8.genericFrame3D.build.RCsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.EQ.bidirect.tcl

puts " ----------------- Reinforced Concrete section ---------------"
puts " --  RC-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex8.genericFrame3D.build.RCsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.sine.multipleSupport.tcl

puts " ----------------- Reinforced Concrete section ---------------"
puts " --  RC-Section: Elastic Uniaxial or Fiber Section, Nonlinear Element --"
puts " --  Multiple-support Earthquake Excitation --"
source Ex8.genericFrame3D.build.RCsec.tcl
source Ex8.genericFrame3D.analyze.Dynamic.eq.multipleSupport.tcl

puts " --  ----------------------------------------------------------------- "


