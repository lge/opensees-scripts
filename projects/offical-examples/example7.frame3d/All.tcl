

puts " ----------------- Steel W section ---------------"
puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Uniform Sine-wave Excitation --"
source Ex7.Frame3D.build.Wsec.tcl
source Ex7.Frame3D.analyze.Dynamic.sine.Uniform.tcl

puts " ----------------- Steel W section ---------------"
puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Uniform Earthquake Excitation --"
source Ex7.Frame3D.build.Wsec.tcl
source Ex7.Frame3D.analyze.Dynamic.EQ.Uniform.tcl
