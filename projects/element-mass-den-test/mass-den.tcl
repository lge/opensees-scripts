wipe;
model basic -ndm 2 -ndf 3
geomTransf Linear 1;
geomTransf PDelta 2;
geomTransf Corotational 3;
node 1 0.0 0.0
node 2 0.0 40.0 -mass 1000 1000 1000;
node 3 40.0 40.0 -mass 1000 1000 1000;
#node 2 0.0 40.0;
#node 3 40.0 40.0;
node 4 40.0 0.0
fix 1 1 1 1;
fix 4 1 1 1;

element elasticBeamColumn 1 1 2 100 29000 833.3333 3;
element elasticBeamColumn 2 2 3 100 29000 833.3333 3;
#element elasticBeamColumn 2 2 3 100 29000 833.3333 3 -mass 50.0 dummy;
element elasticBeamColumn 3 3 4 100 29000 833.3333 3;

puts [eigen 2]
