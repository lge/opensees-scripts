% Richard WOod
% script to perform IDA analysis in PGA stpes
clear, close all, clc

drift_level=0.03;

num_recs=22;
% record_name=[];
name_max=[2,2,2,2,2,2,1,1,2,1,1,2,1,1,1,1,1,2,2,2,1,2];
% for i=1:length(name_max)
% temp=['record' num2str(i) '_' num2str(name_max(i)) '.txt']
% record_name=[record_name,char(temp)];
% end


dt=[0.0100,0.0100,0.0100,0.0100,0.0100,0.00500,0.0100,0.0100,0.00500,0.00500,0.0200,0.00250,0.00500,0.00500,0.0200,0.00500,0.0100,0.0200,0.00500,0.00500,0.0100,0.0100;];

% for i=1:length(num_recs);
for i=1;
%     record=['record' num2str(i) '_1.txt'];
    record=['record' num2str(i) '_' num2str(name_max(i)) '.txt'];
    record_file=dlmread(record);
    record_max=max(abs(record_file));
    record_file=dlmread(record);
    Npoints=length(record_file);
    k=1;
    factor=1/record_max*0.20*k;
    while k~=0;
        directory_name=[char(record(:,1:end-4)) '_iter_' num2str(k)];
        dlmwrite('input_parameters.txt',['set dir ' char(directory_name) ' ;'],'delimiter','','newline','pc')
        dlmwrite('input_parameters.txt',['set scale ' num2str(factor) ' ;'],'delimiter','','newline','pc','-append')
        dlmwrite('input_parameters.txt',['set eq_record ' char(record) ' ;'],'delimiter','','newline','pc','-append')
        dlmwrite('input_parameters.txt',['set Npoints ' num2str(Npoints) ' ;'],'delimiter','','newline','pc','-append')
        dlmwrite('input_parameters.txt',['set dt ' num2str(dt(i)) ' ;'],'delimiter','','newline','pc','-append')
        !OpenSees build.tcl
        % Drift Check------------------------------------------------------
        floor_disp=dlmread([directory_name '/fl_disp.txt']);
        isfloor_1=(floor_disp(:,3)-floor_disp(:,2))/144; max_fl=abs(max(isfloor_1));
        isfloor_2=(floor_disp(:,4)-floor_disp(:,3))/144; max_f2=abs(max(isfloor_2));
        isfloor_3=(floor_disp(:,5)-floor_disp(:,4))/144; max_f3=abs(max(isfloor_3));
        is_env=[max_fl,max_f2,max_f3];
%         is_env=[max_fl,max_f2,max_f3,max_f4,max_f5,max_f6,max_f7,max_f8];
        max_drift=max(is_env);
        %done inputing drift-----------------------------------------------
        if max_drift>drift_level % check is drift level is exceeded
            disp('is drift is sufficent, analysis complete')
%             disp(char(record_name(i,:)))
            disp(['last folder is ' 'record' num2str(i) '_' num2str(k)])
            k=0;
        end
        
        if max_drift<drift_level % upscale factor and repeat if too low
            disp('is drift is too low, repeating analysis')
            factor=factor+0.20;
             if k<36
                k=k+1
            end
            if k>=36
                k=0;
                disp('Iteration too high, quit IDA analysis')
            end
        end
    end
    
end




3


