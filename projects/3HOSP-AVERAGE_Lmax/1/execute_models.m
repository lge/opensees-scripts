
% execute single building model for 3 types: a, b short, b long.  "c"
% rotation released is not considered herein.

type=[1,2,3,4,5,6,7];
nodal=['a','b','b','b','b','b','b'];
PW=[0,1,1,1,1,1,1];
PWType=[0,1,1,1,1,1,1]; %use 0 for commerical, 1 for institutional
IDPW=[0,0,0,-1,-1,1,1]; %where [minu_std,ave,plus_std]=[-1,0,1]
lenwall=[0,43.0,101.0,43.0,101.0,43.0,101.0];
% lenwall=[0,11.5,11.5];
pw_type=['x','b','b','b','b','b','b'];


for i=1:7;
    dlmwrite('input_parameters.txt',['set dir config_' num2str(type(i)) ],'delimiter','','newline','pc')
    dlmwrite('input_parameters.txt',['source nodal_' nodal(i) '.txt' ],'delimiter','','newline','pc','-append')
    dlmwrite('input_parameters.txt',['set PW ' num2str(PW(i)) ],'delimiter','','newline','pc','-append')
    dlmwrite('input_parameters.txt',['set IDPW ' num2str(IDPW(i)) ],'delimiter','','newline','pc','-append')
    dlmwrite('input_parameters.txt',['set PWType ' num2str(PWType(i)) ],'delimiter','','newline','pc','-append')
    dlmwrite('input_parameters.txt',['set lenwall ' num2str(lenwall(i)) ],'delimiter','','newline','pc','-append')
    dlmwrite('input_parameters.txt',['set partition_call partitions_' pw_type(i) '.txt' ],'delimiter','','newline','pc','-append')
    !OpenSees build.tcl
end