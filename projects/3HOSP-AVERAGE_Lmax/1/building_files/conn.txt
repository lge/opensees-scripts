#3-HOSP												
#element	nonlinearBeamColumn	$eleTag	$iNode	$jNode	$numIntgrPts	$secTag	$transfTag	<-mass	$massDens>	<-iter	$maxIters	$tol>
# beams (RIGHT)												
element	nonlinearBeamColumn	101	4	5	3	5001	1	;	#	1		
element	nonlinearBeamColumn	102	7	8	3	5001	1	;	#	2		
element	nonlinearBeamColumn	103	10	11	3	5001	1	;	#	3		
# beams (LEFT)												
element	nonlinearBeamColumn	201	5	6	3	5001	1	;	#	1		
element	nonlinearBeamColumn	202	8	9	3	5001	1	;	#	2		
element	nonlinearBeamColumn	203	11	12	3	5001	1	;	#	3		
# columns												
element	nonlinearBeamColumn	301	1	4	5	6001	1	;	#	1	L	
element	nonlinearBeamColumn	302	3	6	5	6001	1	;	#	1	R	
element	nonlinearBeamColumn	303	4	7	5	6001	1	;	#	2	L	
element	nonlinearBeamColumn	304	6	9	5	6001	1	;	#	2	R	
element	nonlinearBeamColumn	305	7	10	5	6001	1	;	#	3	L	
element	nonlinearBeamColumn	306	9	12	5	6001	1	;	#	3	R	
