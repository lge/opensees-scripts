recorder Node -file $dir/pW_disp.txt -time -node 101 102 201 202 301 302 -dof 1 disp;
recorder Element -file $dir/pw_forces.txt -time -ele 1001 1002 1003 force;
recorder Node -file $dir/is_drift.txt -time -node 1 4 7 10 -dof 1 disp;
