wipe;
model basic -ndm 2 -ndf 3
geomTransf Linear 1;
geomTransf PDelta 2;
geomTransf Corotational 3;

set nz 10;
uniaxialMaterial Elastic 1 29000;

section Fiber 1 {
	patch quad 1 $nz $nz -5.0 -5.0 5.0 -5.0 5.0 5.0 -5.0 5.0;
}

node 1 0.0 0.0
node 2 0.0 100.0 -mass 1000 1000 1000;
node 6 -100.0 100.0 -mass 1000 1000 1000;
node 7 -100.0 0.0 -mass 1000 1000 1000;
element elasticBeamColumn 1 1 2 100 29000 833.3333 1;
element nonlinearBeamColumn 4 1 6 5 1 3;
element dispBeamColumn 5 6 7 5 1 3;
fix 1 1 1 1;
fix 7 1 1 1;

model basic -ndm 2 -ndf 2
uniaxialMaterial Elastic 1 3000

node 3 0.0 100.0;
node 4 100.0 100.0 -mass 1000 1000 1000;
node 5 100.0 0.0
fix 5 1 1;
element Truss 2 3 4 100 1;
element Truss 3 4 5 100 1;

equalDOF 2 3 1 2

# puts [eigen 2]

model basic -ndm 2 -ndf 3
timeSeries Linear 1;

pattern Plain 1 1 {
    load 2 100 0.0 0.0
}
system BandSPD
numberer RCM
constraints Transformation
integrator LoadControl 1.0
algorithm Linear
analysis Static 
recorder Node -file example.out -time -node 4 -dof 1 2 disp
recorder Element -file eleLocal.out -time -ele 1 2 3  basicForces
analyze 1
# puts [json-load]
